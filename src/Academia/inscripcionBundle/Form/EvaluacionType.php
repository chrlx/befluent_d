<?php

namespace Academia\inscripcionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Academia\inscripcionBundle\Entity\Grupo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EvaluacionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $grupo = new Grupo();
        $builder->add('nombre')->add('ponderacion')
        ->add('idGrupo',EntityType::class, array(
            'class' => 'AcademiainscripcionBundle:Grupo',
            'choice_label' =>  function ($grupo) {
                return  $grupo->getTipo() . '  ' . $grupo->getHorario()->format('H:i') . '   -   ' . $grupo->getHorarioFin()->format('H:i');;
                },
            
            'required'=>true

        ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Academia\inscripcionBundle\Entity\Evaluacion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'academia_inscripcionbundle_evaluacion';
    }


}
