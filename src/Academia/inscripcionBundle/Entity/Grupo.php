<?php

namespace Academia\inscripcionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupo
 *
 * @ORM\Table(name="grupo")
 * @ORM\Entity(repositoryClass="Academia\inscripcionBundle\Repository\GrupoRepository")
 */
class Grupo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel", type="string", length=255)
     */
    private $nivel;

    /**
     * @var \Time
     *
     * @ORM\Column(name="horario", type="time")
     */
    private $horario;

    
    /**
     * @var \time
     *
     * @ORM\Column(name="horarioFin", type="time")
     */
    private $horarioFin;

    /**
     * @var string
     *
     * @ORM\Column(name="salon", type="string", length=255)
     */
    private $salon;

    /** 
    * @ORM\ManyToMany(targetEntity="Estudiante", inversedBy="grupos")
     * })
    */
    private $estudiantes;

    #relacion con la entidad usuario

    /**
    * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="grupos")
    * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="SET NULL")
    * 
     */
    private $coach;
    
    /**
     * @var string
     *
     * @ORM\Column(name="modalidad", type="string", length=12)
     */

    private $modalidad;

     /**
      * @ORM\OneToMany(targetEntity="Evaluacion", mappedBy="idGrupo")
    */
    private $evaluacion;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Grupo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     *
     * @return Grupo
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return string
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set horario
     *
     * @param \DateTime $horario
     *
     * @return Grupo
     */
    public function setHorario($horario)
    {
        $this->horario = $horario;

        return $this;
    }

    /**
     * Get horario
     *
     * @return \DateTime
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * Set salon
     *
     * @param string $salon
     *
     * @return Grupo
     */
    public function setSalon($salon)
    {
        $this->salon = $salon;

        return $this;
    }

    /**
     * Get salon
     *
     * @return string
     */
    public function getSalon()
    {
        return $this->salon;
    }

    //set y get estudiantes autogenerados en doctrine

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->estudiantes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add estudiante
     *
     * @param \Academia\inscripcionBundle\Entity\Estudiante $estudiante
     *
     * @return Grupo
     */
    public function addEstudiante(\Academia\inscripcionBundle\Entity\Estudiante $estudiante)
    {
        $this->estudiantes[] = $estudiante;

        return $this;
    }

    /**
     * Remove estudiante
     *
     * @param \Academia\inscripcionBundle\Entity\Estudiante $estudiante
     */
    public function removeEstudiante(\Academia\inscripcionBundle\Entity\Estudiante $estudiante)
    {
        $this->estudiantes->removeElement($estudiante);
    }

    /**
     * Get estudiantes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEstudiantes()
    {
        return $this->estudiantes;
    }

    public function __toString(){
        return $this->tipo;
    }




    /**
     * Set docente
     *
     * @param \Academia\inscripcionBundle\Entity\Docente $docente
     *
     * @return Grupo
     */
    public function setDocente(\Academia\inscripcionBundle\Entity\Docente $docente = null)
    {
        $this->docente = $docente;

        return $this;
    }

    /**
     * Get docente
     *
     * @return \Academia\inscripcionBundle\Entity\Docente
     */
    public function getDocente()
    {
        return $this->docente;
    }

    /**
     * Set iddocente
     *
     * @param \Academia\inscripcionBundle\Entity\Docente $iddocente
     *
     * @return Grupo
     */
    public function setIddocente(\Academia\inscripcionBundle\Entity\Docente $iddocente = null)
    {
        $this->iddocente = $iddocente;

        return $this;
    }

    /**
     * Get iddocente
     *
     * @return \Academia\inscripcionBundle\Entity\Docente
     */
    public function getIddocente()
    {
        return $this->iddocente;
    }

    /**
     * Set horarioFin
     *
     * @param \DateTime $horarioFin
     *
     * @return Grupo
     */
    public function setHorarioFin($horarioFin)
    {
        $this->horarioFin = $horarioFin;

        return $this;
    }

    /**
     * Get horarioFin
     *
     * @return \DateTime
     */
    public function getHorarioFin()
    {
        return $this->horarioFin;
    }

    /**
     * Set modalidad
     *
     * @param string $modalidad
     *
     * @return Grupo
     */
    public function setModalidad($modalidad)
    {
        $this->modalidad = $modalidad;

        return $this;
    }

    /**
     * Get modalidad
     *
     * @return string
     */
    public function getModalidad()
    {
        return $this->modalidad;
    }

    /**
     * Add evaluacion
     *
     * @param \Academia\inscripcionBundle\Entity\Evaluacion $evaluacion
     * @return Grupo
     */
    public function addEvaluacion(\Academia\inscripcionBundle\Entity\Evaluacion $evaluacion)
    {
        $this->evaluacion[] = $evaluacion;

        return $this;
    }

    /**
     * Remove evaluacion
     *
     * @param \Academia\inscripcionBundle\Entity\Evaluacion $evaluacion
     */
    public function removeEvaluacion(\Academia\inscripcionBundle\Entity\Evaluacion $evaluacion)
    {
        $this->evaluacion->removeElement($evaluacion);
    }

    /**
     * Get evaluacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvaluacion()
    {
        return $this->evaluacion;
    }

    /** Estos metodos se crean automaticamente con doctrine lo que implica la relacion */

    /**
     * Set coach
     *
     * @param \Academia\inscripcionBundle\Entity\Usuario $coach
     *
     * @return Grupo
     */
    public function setCoach(\Academia\inscripcionBundle\Entity\Usuario $coach = null)
    {
        $this->coach = $coach;

        return $this;
    }

    /**
     * Get coach
     *
     * @return \Academia\inscripcionBundle\Entity\Usuario
     */
    public function getCoach()
    {
        return $this->coach;
    }

    /** *************************************************************************** */
}
