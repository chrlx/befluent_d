<?php

namespace Academia\inscripcionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Academia\inscripcionBundle\Inscripcion;

/**
 * Estudiante
 *
 * @ORM\Table(name="estudiante")
 * @ORM\Entity(repositoryClass="Academia\inscripcionBundle\Repository\EstudianteRepository")
 */
class Estudiante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="edad", type="integer")
     */
    private $edad;

    /**
     * @var string
     *
     * @ORM\Column(name="dui", type="string", length=255)
     */
    private $dui;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /** 
    * @ORM\ManyToMany(targetEntity="Grupo", inversedBy="estudiantes")
     * })
    */

    private $grupos;

    /**
    *@ORM\OneToOne(targetEntity="Inscripcion",mappedBy="estudiante")
    */
    private $inscripcion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Estudiante
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     *
     * @return Estudiante
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return int
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set dui
     *
     * @param string $dui
     *
     * @return Estudiante
     */
    public function setDui($dui)
    {
        $this->dui = $dui;

        return $this;
    }

    /**
     * Get dui
     *
     * @return string
     */
    public function getDui()
    {
        return $this->dui;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Estudiante
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Estudiante
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    // set y get de Grupo autogenerados con doctrine

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add grupo
     *
     * @param \Academia\inscripcionBundle\Entity\Grupo $grupo
     *
     * @return Estudiante
     */
    public function addGrupo(\Academia\inscripcionBundle\Entity\Grupo $grupo)
    {
        $this->grupos[] = $grupo;

        return $this;
    }

    /**
     * Remove grupo
     *
     * @param \Academia\inscripcionBundle\Entity\Grupo $grupo
     */
    public function removeGrupo(\Academia\inscripcionBundle\Entity\Grupo $grupo)
    {
        $this->grupos->removeElement($grupo);
    }

    /**
     * Get grupos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrupos()
    {
        return $this->grupos;
    }

    public function __toString(){
        return $this->nombre;
    }

    /**
     * Set inscripcion
     *
     * @param \Academia\inscripcionBundle\Entity\Inscripcion $inscripcion
     *
     * @return Estudiante
     */
    public function setInscripcion(\Academia\inscripcionBundle\Entity\Inscripcion $inscripcion = null)
    {
        $this->inscripcion = $inscripcion;

        return $this;
    }

    /**
     * Get inscripcion
     *
     * @return \Academia\inscripcionBundle\Entity\Inscripcion
     */
    public function getInscripcion()
    {
        return $this->inscripcion;
    }
}
