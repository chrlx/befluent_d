<?php

namespace Academia\inscripcionBundle\Entity;

//use Academia\inscripcionBundle\Estudiante;
use Doctrine\ORM\Mapping as ORM;

/**
 * Inscripcion
 *
 * @ORM\Table(name="inscripcion")
 * @ORM\Entity
 */
class Inscripcion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDINSCRIPCION", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idinscripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FECHA", type="datetime", nullable=false)
     */
    private $fecha = 'CURRENT_TIMESTAMP';

    /**
    * @ORM\OneToOne(targetEntity="Estudiante",inversedBy="inscripcion")
    * @ORM\JoinColumn(name="IDESTUDIANTE",referencedColumnName="id",onDelete="CASCADE")
    */

    private $estudiante;


    /**
     * Get idinscripcion
     *
     * @return integer
     */
    public function getIdinscripcion()
    {
        return $this->idinscripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Inscripcion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

     public function setEstudiante($estudiante)
    {
        $this->estudiante = $estudiante;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getEstudiante()
    {
        return $this->estudiante;
    }
}
