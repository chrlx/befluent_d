<?php

namespace Academia\inscripcionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Docente
 *
 * @ORM\Table(name="docente")
 * @ORM\Entity
 */
class Docente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDDOCENTE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddocente;

    /**
     * @var string
     *
     * @ORM\Column(name="NOMBRE", type="string", length=1024, nullable=true)
     */
    private $nombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="EDAD", type="integer", nullable=true)
     */
    private $edad;

    /**
     * @var string
     *
     * @ORM\Column(name="DUI", type="string", length=10, nullable=true)
     */
    private $dui;

    /**
     * @var string
     *
     * @ORM\Column(name="TELEFONO", type="string", length=8, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=1024, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="ESPECIALIZACION", type="string", length=1024, nullable=true)
     */
    private $especializacion;


    /**
     * Get iddocente
     *
     * @return integer
     */
    public function getIddocente()
    {
        return $this->iddocente;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Docente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     *
     * @return Docente
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return integer
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set dui
     *
     * @param string $dui
     *
     * @return Docente
     */
    public function setDui($dui)
    {
        $this->dui = $dui;

        return $this;
    }

    /**
     * Get dui
     *
     * @return string
     */
    public function getDui()
    {
        return $this->dui;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Docente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Docente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set especializacion
     *
     * @param string $especializacion
     *
     * @return Docente
     */
    public function setEspecializacion($especializacion)
    {
        $this->especializacion = $especializacion;

        return $this;
    }

    /**
     * Get especializacion
     *
     * @return string
     */
    public function getEspecializacion()
    {
        return $this->especializacion;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->grupo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add grupo
     *
     * @param \Academia\inscripcionBundle\Entity\Grupo $grupo
     *
     * @return Docente
     */
    public function addGrupo(\Academia\inscripcionBundle\Entity\Grupo $grupo)
    {
        $this->grupo[] = $grupo;

        return $this;
    }

    /**
     * Remove grupo
     *
     * @param \Academia\inscripcionBundle\Entity\Grupo $grupo
     */
    public function removeGrupo(\Academia\inscripcionBundle\Entity\Grupo $grupo)
    {
        $this->grupo->removeElement($grupo);
    }

    /**
     * Get grupo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    public function __toString(){
        return $this->nombre;
    }
}
