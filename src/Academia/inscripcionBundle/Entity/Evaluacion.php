<?php

namespace Academia\inscripcionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evaluacion
 *
 * @ORM\Table(name="evaluacion")
 * @ORM\Entity(repositoryClass="Academia\inscripcionBundle\Repository\EvaluacionRepository")
 */
class Evaluacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=30)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="ponderacion", type="integer")
     */
    private $ponderacion;

    /**
    * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="evaluacion")
    * @ORM\JoinColumn(name="idGrupo", referencedColumnName="id", onDelete="SET NULL")
    * 
     */

    private $idGrupo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Evaluacion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ponderacion
     *
     * @param integer $ponderacion
     * @return Evaluacion
     */
    public function setPonderacion($ponderacion)
    {
        $this->ponderacion = $ponderacion;

        return $this;
    }

    /**
     * Get ponderacion
     *
     * @return integer 
     */
    public function getPonderacion()
    {
        return $this->ponderacion;
    }

    /**
     * Set idGrupo
     *
     * @param \Academia\inscripcionBundle\Entity\Grupo $idGrupo
     * @return Evaluacion
     */
    public function setIdGrupo(\Academia\inscripcionBundle\Entity\Grupo $idGrupo = null)
    {
        $this->idGrupo = $idGrupo;

        return $this;
    }

    /**
     * Get idGrupo
     *
     * @return \Academia\inscripcionBundle\Entity\Grupo 
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }
}
