<?php

namespace Academia\inscripcionBundle\Controller;

use Academia\inscripcionBundle\Entity\Evaluacion;
use Academia\inscripcionBundle\Entity\Grupo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Form;

/**
 * Evaluacion controller.
 *
 */
class EvaluacionController extends Controller
{
    /**
     * Lists all evaluacion entities.
     *
     */

   
  

    public function indexAction(Grupo $grupo,Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $evaluacions = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $grupo->getId() ));

         $dql="SELECT d FROM AcademiainscripcionBundle:Evaluacion d";
        $evaluaciones= $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($evaluaciones, $request->query->getInt('page', 1), 5);



        return $this->render('evaluacion/index.html.twig', array(
            'evaluacions' => $evaluacions,
            'pagination'=>$pagination,
        ));
    }

    /**
     * Creates a new evaluacion entity.
     *
     */
    public function newAction(Request $request)
    {   

        if($request->isXmlHttpRequest()){

        $i=0;
        $nombre=$request->request->get("nombre");
        $ponderacion=$request->request->get("ponderacion");
        $grupo=$request->request->get("grupo");
        $tam=$request->request->get("tam");
        $em = $this->getDoctrine()->getManager();
  

        $grupoObtenido=$em->getRepository('AcademiainscripcionBundle:Grupo')->findBy(array('id' => $grupo ));// Se obtiene el grupo al que se le van agregar las evaluaciones
        $grupoP=   $grupoObtenido[0];


        while ($i<$tam ) {
            # code...
            $evaluaciones=new Evaluacion();
            $evaluaciones->setNombre($nombre[$i]);
            $evaluaciones->setPonderacion($ponderacion[$i]);
            $evaluaciones->setIdGrupo($grupoP);
            $em->persist($evaluaciones);
            $em->flush();
            $i++;
        }

       
        
        $eval[$i]=$evaluaciones;
        $i++;

        $jsonData = array();
          
                # code...
                $temp = array(
                'nombre' => $nombre,  
                'ponderacion' => $ponderacion);  
         
                $jsonData[0] = $temp;

            
            return new JsonResponse($jsonData);

        }
        else{
        $eval=array();
        $i=0;
        $evaluacion = new Evaluacion();
        $form = $this->createForm('Academia\inscripcionBundle\Form\EvaluacionType', $evaluacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($evaluacion);
            $em->flush();

            return $this->redirectToRoute('evaluacion_show', array('id' => $evaluacion->getId()));
        }

        return $this->render('evaluacion/new.html.twig', array(
            'evaluacion' => $evaluacion,
            'form' => $form->createView(),
        ));
    }}

    /**
     * Finds and displays a evaluacion entity.
     *
     */
    public function showAction(Grupo $grupo,Request $request)
    {
        $idGr=$grupo->getId(); 
        $em = $this->getDoctrine()->getManager();
        $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $idGr ));

        $tamaño=sizeof($evaluaciones);

        $editForm = $this->createFormBuilder($evaluaciones);
        $editForm = $editForm->getForm();
        $editForm->handleRequest($request);
 
        //$paginator = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($evaluaciones, $request->query->getInt('page', 1), 5);

        if ($editForm->isSubmitted()&&$editForm->isValid()) {
            $i=0;
                    foreach ($evaluaciones as $valor) {
                         $nombre= $request->request->get($evaluaciones[$i]->getNombre().$evaluaciones[$i]->getId());
                         $ponderacion= $request->request->get($evaluaciones[$i]->getId());
                         $id=$evaluaciones[$i]->getIdGrupo();

                         $em = $this->getDoctrine()->getManager();
                         $evaluacionViejo = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->find($evaluaciones[$i]->getId());
                         $evaluacionViejo->setNombre($nombre);
                         $evaluacionViejo->setPonderacion($ponderacion);
                         $em->flush();
                         //$evaluacionEdita->setIdGrupo($evaluaciones[$i]->getIdGrupo());
                        
                    
                         //$consulta = $em->createQuery('UPDATE AcademiainscripcionBundle:Evaluacion d SET d.nombre = :nombre WHERE d.id = :id');
            
                         //$consulta->setParameters(array('nombre' => $evaluaciones[$i]->getIdGrupo(),'id'  => $nombre,));


                        $i=$i+1;

                    }

            return $this->redirectToRoute('evaluacion_show',array('id'=>$grupo->getId()));
            # code...
        }

        return $this->render('evaluacion/show.html.twig', array(
            //'pagination'=>$pagination,
            'evaluaciones'=>$evaluaciones,
            'editForm'=>$editForm->createView(),
            'tamaño'=>$tamaño,
        ));


    }

    /**
     * Displays a form to edit an existing evaluacion entity.
     *
     */
    public function editAction(Evaluacion $evaluacion)
    {
        $deleteForm = $this->createDeleteForm($evaluacion);
         $em = $this->getDoctrine()->getManager();
         $grupoDeEvaluacion = $em->getRepository('AcademiainscripcionBundle:Grupo')->findBy(array("id"=>$evaluacion->getIdGrupo()->getId() ));       

        return $this->render('evaluacion/edit.html.twig', array(
            'evaluacion' => $evaluacion,
            'delete_form' => $deleteForm->createView(),
            'grupoDeEvaluacion'=>$grupoDeEvaluacion[0],
        ));
    }

    /**
     * Deletes a evaluacion entity.
     *
     */
    public function deleteAction(Request $request, Evaluacion $evaluacion)
    {   
        //$id=$evaluacion->getId();
       //$grupo = $this->doctrine->em->find('AcademiainscripcionBundle:Grupo', $id);

        $form = $this->createDeleteForm($evaluacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($evaluacion);
            $em->flush();
        }

        return $this->redirectToRoute('evaluacion_show',array('id'=>$evaluacion->getIdGrupo()->getId()));
    }

    /**
     * Creates a form to delete a evaluacion entity.
     *
     * @param Evaluacion $evaluacion The evaluacion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Evaluacion $evaluacion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('evaluacion_delete', array('id' => $evaluacion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function obtenerAction(Request $request){
  
        $idDelGrupo=$request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $evaluacion = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => 1 ));  
        
        if($request->isXmlHttpRequest()){

        $jsonData= array();
        $idx=0;
          foreach ($evaluacion as $evaluacion) {
            $temp = array(
                'id' => $evaluacion->getId(),
                'nombre'=>$evaluacion->getNombre(),
                'ponderacion'=>$evaluacion->getPonderacion(),
            );  

                $jsonData[$idx++] = $temp;
        

            }
            return new JsonResponse($jsonData);
    }else { 
      return $this->render('student/ajax.html.twig'); 
   } 
    }


public function crearAction(Grupo $grupo,Request $request){
        $em = $this->getDoctrine()->getManager();

        $evaluacions = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->find(1);
      

        return $this->render('evaluacion/new.html.twig', array(
            'evaluaciones'=>$evaluacions,

        ));
    }

public function agregarEvaluaciones(Request $request){
    if($request->isXmlHttpRequest()){

        $i=0;
        $nombre=$request->request->get("nombre");
        $ponderacion=$request->request->get("ponderacion");
        $grupoT=$request->request->get("grupo");
        $tam=$request->request->get("tam");
        $em = $this->getDoctrine()->getManager();
  

        $grupoObtenido=$em->getRepository('AcademiainscripcionBundle:Grupo')->findBy(array('id' => 1 ));// Se obtiene el grupo al que se le van agregar las evaluaciones
        $grupoP=   $grupoObtenido[0];


        while ($i<$tam ) {
            # code...
            $evaluaciones=new Evaluacion();
            $evaluaciones->setNombre($nombre[$i]);
            $evaluaciones->setPonderacion($ponderacion[$i]);
            $evaluaciones->setIdGrupo($grupoP);
            $em->persist($evaluaciones);
            $em->flush();
            $i++;
        }
        $eval[$i]=$evaluaciones;
        $i++;

        $jsonData = array();
          
                # code...
                $temp = array(
                'nombre' => $nombre,  
                'ponderacion' => $ponderacion);  
         
                $jsonData[0] = $temp;

            
            return new JsonResponse($jsonData);

        }
}
}