<?php

namespace Academia\inscripcionBundle\Controller;

use Academia\inscripcionBundle\Entity\Grupo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Grupo controller.
 *
 */
class GrupoController extends Controller
{
    /**
     * Lists all grupo entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //$grupos = $em->getRepository('AcademiainscripcionBundle:Grupo')->findAll();

        $dql="SELECT g FROM AcademiainscripcionBundle:Grupo g";
        $grupos= $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);

        /*
        return $this->render('grupo/index.html.twig', array(
            'grupos' => $grupos,
        ));
        */
        return $this->render('grupo/index.html.twig', array(
            'pagination' => $pagination,
        ));


    }

    /**
     * metodo para recuperar los grupos asignados al usuario
     */
    public function misgruposAction(Request $request)
    {
        $idUsuario = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT g FROM AcademiainscripcionBundle:Grupo g JOIN g.coach u WHERE u.id = :idUsuario";

        $misgrupos = $em->createQuery($dql)->setParameter('idUsuario', $idUsuario);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($misgrupos, $request->query->getInt('page', 1), 5);

        return $this->render('grupo/misgrupos.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new grupo entity.
     *
     */
    public function newAction(Request $request)
    {
        $grupo = new Grupo();
        $form = $this->createForm('Academia\inscripcionBundle\Form\GrupoType', $grupo);
        $form->handleRequest($request);
        $horariosEstanBien=true;

        $docenteOcupado=false;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

 //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 $gps = $em->getRepository('AcademiainscripcionBundle:Grupo')->findBy(array('coach' => $grupo->getId() , )); // Obtiene todos los grupos que tiene el docente escogido

        for($i=0; $i<sizeof($gps);$i++){//Compara las horas que ya imparte con la del nuevo grupo que se creara
            $horarioImpartidos=$gps[$i]->getHorario();// convierte el formato time a string
            $nuevoHorario=$grupo->getHorario();
            $horarioImpartidosFin= $gps[$i]->getHorarioFin();
            $nuevoHorarioFin=$grupo->getHorarioFin();

            $nuevaModalidad = $grupo->getModalidad();
            $modalidadImpartida = $gps[$i]->getModalidad();

            if($nuevoHorario == $horarioImpartidos || $nuevoHorarioFin== $horarioImpartidosFin  && $nuevaModalidad==$modalidadImpartida){
                $docenteOcupado=true;
                }
                elseif($nuevoHorario>$horarioImpartidos&&$nuevoHorarioFin<$horarioImpartidosFin&&$nuevaModalidad==$modalidadImpartida){
                    $docenteOcupado=true;
                }
                elseif ($nuevoHorario<$horarioImpartidos&&$horarioImpartidosFin<$nuevoHorarioFin&&$nuevaModalidad==$modalidadImpartida) {
                    # code...
                    $docenteOcupado=true;
                }elseif($nuevoHorario>$horarioImpartidos&&$nuevoHorario<$horarioImpartidosFin&&$nuevaModalidad==$modalidadImpartida){
                    $docenteOcupado=true;
                }
                elseif ($nuevoHorarioFin>$horarioImpartidos&&$nuevoHorarioFin<$horarioImpartidosFin&&$nuevaModalidad==$modalidadImpartida) {
                    # code...
                    $docenteOcupado=true;
                }
            }

            $nuevoHorarioFin=$grupo->getHorarioFin();
            $nuevoHorario=$grupo->getHorario();
            $horariosEstanBien=$this->horariosBien($nuevoHorario,$nuevoHorarioFin);

            if($docenteOcupado==false && $horariosEstanBien==true){
                $em->persist($grupo);
                $em->flush();
                return $this->redirectToRoute('grupo_show', array('id' => $grupo->getId()));
            }
             return $this->render('grupo/new.html.twig', array(
            'grupo' => $grupo,
            'form' => $form->createView(),
            'docenteOcupado'=> $docenteOcupado,
            'horariosEstanBien'=>$horariosEstanBien));
        }

        return $this->render('grupo/new.html.twig', array(
            'grupo' => $grupo,
            'form' => $form->createView(),
            'docenteOcupado'=> $docenteOcupado,
            'horariosEstanBien'=>$horariosEstanBien
        ));
    }

    /**
     * Finds and displays a grupo entity.
     *
     */
    public function showAction(Grupo $grupo)
    {
        $deleteForm = $this->createDeleteForm($grupo);

        return $this->render('grupo/show.html.twig', array(
            'grupo' => $grupo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing grupo entity.
     *
     */
    public function editAction(Request $request, Grupo $grupo)
    {
        $deleteForm = $this->createDeleteForm($grupo);
        $editForm = $this->createForm('Academia\inscripcionBundle\Form\GrupoType', $grupo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('grupo_edit', array('id' => $grupo->getId()));
        }

        return $this->render('grupo/edit.html.twig', array(
            'grupo' => $grupo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a grupo entity.
     *
     */
    public function deleteAction(Request $request, Grupo $grupo)
    {
        $form = $this->createDeleteForm($grupo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($grupo);
            $em->flush();
        }

        return $this->redirectToRoute('grupo_index');
    }

    /**
     * Creates a form to delete a grupo entity.
     *
     * @param Grupo $grupo The grupo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Grupo $grupo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grupo_delete', array('id' => $grupo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function horariosBien(\DateTime $horaInicio, \Datetime $horaFin)
    {
        $horaBien=true;
        if($horaInicio>$horaFin || $horaInicio==$horaFin){
            $horaBien=false;
        }
        return $horaBien;
    }

    public function consultaInglesAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $sql="SELECT g FROM AcademiainscripcionBundle:Grupo g WHERE g.tipo='Inglés'";
      $grupos= $em->createQuery($sql);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);
      return $this->render('grupo/index.html.twig', array(
          'pagination' => $pagination,
      ));
    }

    public function consultaFrancesAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $sql="SELECT g FROM AcademiainscripcionBundle:Grupo g WHERE g.tipo='Francés'";
      $grupos= $em->createQuery($sql);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);
      return $this->render('grupo/index.html.twig', array(
          'pagination' => $pagination,
      ));
    }

    public function consultaInglesBasicoAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $sql="SELECT g FROM AcademiainscripcionBundle:Grupo g WHERE g.tipo='Inglés' AND g.nivel='Básico'";
      $grupos= $em->createQuery($sql);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);
      return $this->render('grupo/index.html.twig', array(
          'pagination' => $pagination,
      ));
    }

    public function consultaInglesIntermedioAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $sql="SELECT g FROM AcademiainscripcionBundle:Grupo g WHERE g.tipo='Inglés' AND g.nivel='Intermedio'";
      $grupos= $em->createQuery($sql);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);
      return $this->render('grupo/index.html.twig', array(
          'pagination' => $pagination,
      ));
    }

    public function consultaInglesAvanzadoAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $sql="SELECT g FROM AcademiainscripcionBundle:Grupo g WHERE g.tipo='Inglés' AND g.nivel='Avanzado'";
      $grupos= $em->createQuery($sql);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);
      return $this->render('grupo/index.html.twig', array(
          'pagination' => $pagination,
      ));
    }

    public function consultaFrancesBasicoAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $sql="SELECT g FROM AcademiainscripcionBundle:Grupo g WHERE g.tipo='Francés' AND g.nivel='Básico'";
      $grupos= $em->createQuery($sql);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);
      return $this->render('grupo/index.html.twig', array(
          'pagination' => $pagination,
      ));
    }

    public function consultaFrancesIntermedioAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $sql="SELECT g FROM AcademiainscripcionBundle:Grupo g WHERE g.tipo='Francés' AND g.nivel='Intermedio'";
      $grupos= $em->createQuery($sql);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);
      return $this->render('grupo/index.html.twig', array(
          'pagination' => $pagination,
      ));
    }

    public function consultaFrancesAvanzadoAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $sql="SELECT g FROM AcademiainscripcionBundle:Grupo g WHERE g.tipo='Francés' AND g.nivel='Avanzado'";
      $grupos= $em->createQuery($sql);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate($grupos, $request->query->getInt('page', 1), 5);
      return $this->render('grupo/index.html.twig', array(
          'pagination' => $pagination,
      ));
    }

    public function listadoGrupoAction(Request $request, $id)
    {
        /*
        $em = $this->getDoctrine()->getManager();
        $dql="SELECT e.id, e.nombre, e.edad, e.dui, e.telefono, e.mail from AcademiainscripcionBundle:Estudiante e INNER JOIN AcademiainscripcionBundle:Estudiante_Grupo ge ON e.id = ge.id_estudiante INNER JOIN AcademiainscrpcionBundle:grupo g ON g.id = ge.id_grupo WHERE e.id = 1";
        $estudiantes= $em->createQuery($dql);
        */
        $em = $this->getDoctrine()->getEntityManager();
        $db = $em->getConnection();
        $query = "SELECT e.id, e.nombre, e.edad, e.dui, e.telefono, e.email from estudiante e INNER JOIN estudiante_grupo ge ON e.id = ge.estudiante_id INNER JOIN grupo g ON g.id = ge.grupo_id WHERE g.id = $id";
        
        $stmt = $db->prepare($query);
        $params = array();
        $stmt->execute($params);
        $po=$stmt->fetchAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($po, $request->query->getInt('page', 1), 5);

        return $this->render('estudiante/listadoPorGrupo.html.twig', array(
            'pagination' => $pagination,
        ));
    }

}
