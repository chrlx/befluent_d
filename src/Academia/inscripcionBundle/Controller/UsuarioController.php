<?php

namespace Academia\inscripcionBundle\Controller;

use Academia\inscripcionBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
#******************* para validar el campo password  desde el controlador cuando se crea uno nuevo
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError;

use Symfony\Component\HttpFoundation\Request;

/**
 * Usuario controller.
 *
 */
class UsuarioController extends Controller
{
    #este metodo es para que dirija a la pagina home despues de loguearse

    public function homeAction()
    {
        return $this->render('usuario/home.html.twig');
    }

    /**
     * Lists all usuario entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $usuarios = $em->getRepository('AcademiainscripcionBundle:Usuario')->findAll();

        return $this->render('usuario/index.html.twig', array(
            'usuarios' => $usuarios,
        ));
    }

    /**
     * Creates a new usuario entity.
     *
     */
    public function newAction(Request $request)
    {
        $usuario = new Usuario();
        $form = $this->createForm('Academia\inscripcionBundle\Form\UsuarioType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** agregar estas lineas  */
            $password = $form->get('password')->getData();

            /** para controlar la validacion del campo password de un nuevo usuario */
            $passwordConstraint = new Assert\NotBlank();
            $errorList = $this->get('validator')->validate($password, $passwordConstraint);

            if(count($errorList) == 0){
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($usuario, $password);    
                $usuario->setPassword($encoded);


                $em = $this->getDoctrine()->getManager();
                $em->persist($usuario);
                $em->flush();

                return $this->redirectToRoute('usuario_show', array('id' => $usuario->getId()));
            }
            else
            {
                $errorMessage = new FormError($errorList[0]->getMessage());
                $form->get('password')->addError($errorMessage);
            }
        }

        return $this->render('usuario/new.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a usuario entity.
     *
     */
    public function showAction(Usuario $usuario)
    {
        $deleteForm = $this->createDeleteForm($usuario);

        return $this->render('usuario/show.html.twig', array(
            'usuario' => $usuario,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing usuario entity.
     *
     */
    public function editAction(Request $request, Usuario $usuario)
    {
        $deleteForm = $this->createDeleteForm($usuario);
        $editForm = $this->createForm('Academia\inscripcionBundle\Form\UsuarioType', $usuario);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            # para recuperar el password cuando se actualiza
            $password = $editForm->get('password')->getData();
            
            /** si el campo password esta lleno, establece el nuevo password */
            if(!empty($password))
            {
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($usuario, $password);
                $usuario->setPassword($encoded);
            }

            /** si no se edita el campo password, se recupera el password anterior */
            else
            {
                $recoverPass = $this->recoverPass($id);
                $usuario->setPassword($recoverPass[0]['password']);                
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();

            return $this->redirectToRoute('usuario_edit', array('id' => $usuario->getId()));

            #$this->getDoctrine()->getManager()->flush();

            #return $this->redirectToRoute('usuario_edit', array('id' => $usuario->getId()));
        }

        return $this->render('usuario/edit.html.twig', array(
            'usuario' => $usuario,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Recover pass
     * 
     * 
     */

    private function recoverPass($id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT u.password
            FROM AcademiainscripcionBundle:Usuario u
            WHERE u.id = :id'    
        )->setParameter('id', $id);
        
        $currentPass = $query->getResult();
        
        return $currentPass;
    }



    /**
     * Deletes a usuario entity.
     *
     */
    public function deleteAction(Request $request, Usuario $usuario)
    {
        $form = $this->createDeleteForm($usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($usuario);
            $em->flush();
        }

        return $this->redirectToRoute('usuario_index');
    }

    /**
     * Creates a form to delete a usuario entity.
     *
     * @param Usuario $usuario The usuario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Usuario $usuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuario_delete', array('id' => $usuario->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * consulta por tipo de usuario
     */

    public function consultaUsuarioAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sql="SELECT u FROM AcademiainscripcionBundle:Usuario u WHERE u.role='ROLE_ADMIN' ";
        $usuariosAdmin= $em->createQuery($sql);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($usuariosAdmin, $request->query->getInt('page', 1), 5);
        return $this->render('usuario/usuarioadmin.html.twig', array(
            'pagination' => $pagination,
      ));
      
        
    }

    public function consultaUsuarioDocenteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sql="SELECT u FROM AcademiainscripcionBundle:Usuario u WHERE u.role='ROLE_USER' ";
        $usuariosDocente= $em->createQuery($sql);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($usuariosDocente, $request->query->getInt('page', 1), 5);
        return $this->render('usuario/usuarioadmin.html.twig', array(
            'pagination' => $pagination,
      ));
    }

    public function consultaUsuarioRecepcionistaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sql="SELECT u FROM AcademiainscripcionBundle:Usuario u WHERE u.role='ROLE_RECEP' ";
        $usuariosRecep= $em->createQuery($sql);

        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($usuariosRecep, $request->query->getInt('page', 1), 5);
        return $this->render('usuario/usuarioadmin.html.twig', array(
            'pagination' => $pagination,
      ));
      
    }
}