<?php

/* base.html.twig */
class __TwigTemplate_ff3d457e7030a30273033dbe57c6df99ec02088e42e1e0178f6b4d021b7d8bc0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/css/adminlte.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/css/font-awesome.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/css/mystyle.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/iCheck/flat/blue.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/morris/morris.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/jvectormap/jquery-jvectormap-1.2.2.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/datepicker/datepicker3.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/datatables/dataTables.bootstrap4.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("https://use.fontawesome.com/releases/v5.3.1/css/all.css"), "html", null, true);
        echo "\" integrity=\"sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.7/css/mdb.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/timepicker/bootstrap-timepicker.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/timepicker/bootstrap-timepicker.css"), "html", null, true);
        echo "\">
        <style media=\"screen\">


          li {list-style-type: none;}
        </style>
        ";
        // line 29
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 30
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("sigaabef.ico"), "html", null, true);
        echo "\" />
    </head>
    <body onload=\"menuDesplegar()\">
     <aside class=\"main-sidebar sidebar-dark-primary elevation-4\">
       <a href=\"#\" class=\"brand-link\">
       <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("sigaabef.png"), "html", null, true);
        echo "\" alt=\"befluent\" class=\"brand-image img-circle elevation-3\" style=\"opacity: .8\">
       <span class=\"brand-text font-weight-light\">BeFluent</span>
      </a>


   <!-- Sidebar -->
   <div class=\"sidebar\">
     <!-- Sidebar user panel (optional) -->
     <div class=\"user-panel mt-2 pb-2 mb-2 d-flex\">
       <div class=\"image\">
         <img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("sigaabef.png"), "html", null, true);
        echo "\" class=\"img-circle elevation-2\" alt=\"User Image\">
       </div>

      <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("academiainscripcion_homepage");
        echo "\" title=\"Home\"> <div class=\"info text-white\">
       ";
        // line 49
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_ADMIN")) {
            // line 50
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Administrador"), "html", null, true);
            echo "
            ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 51
(isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_USER")) {
            // line 52
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Profesor"), "html", null, true);
            echo "
            ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 53
(isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_RECEP")) {
            // line 54
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Recepcionista"), "html", null, true);
            echo "
            ";
        }
        // line 56
        echo "       </div>
       </a>
     </div>

     <!-- Sidebar Menu -->
     <nav class=\"mt-2\">
     ";
        // line 63
        echo "     ";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 64
            echo "       <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">
        <!--Navegacion de ususarios-->
         <li class=\"nav-item\">
           <a href=\"";
            // line 67
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
            echo "\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-user\"></i>
             <p>Usuarios
             <i class=\"fa fa-angle-left right\"></i></p>
           </a>
           <ul  class=\"nav nav-treeview\">
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-users-cog\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"";
            // line 75
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_consultaUsuarioAdmin");
            echo "\">Administrador</a>
             </li>
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-chalkboard-teacher\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"";
            // line 79
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_consultaUsuarioDocente");
            echo "\">Docentes</a>
             </li>
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-user-alt\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"";
            // line 83
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_consultaUsuarioRecepcionista");
            echo "\">Recepcionistas</a>
             </li>
           </ul>
         </li>
         <!--Navegacion de Grupos-->
         <li class=\"nav-item has-treeview toc-entry toc-h2\">
           <a href=\"#\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-group\"></i>
             <p>Grupo
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
            <ul class=\"nav nav-treeview\">
              <li class=\"nav-item\">
                <i class=\"nav-icon fab fa-etsy\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 98
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_consultaIngles");
            echo "\">Inglés</a>
              </li>
            <ul>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-empty \" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 103
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_consultaInglesBasico");
            echo "\">Básico</a>
              </li>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-half\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 107
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_consultaInglesIntermedio");
            echo "\">Intermedio</a>
              </li>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-full\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 111
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_consultaInglesAvanzado");
            echo "\">Avanzado</a>
              </li>
            </ul>
              <li class=\"nav-item\">
                <i class=\"fas fa-map-pin nav-icon\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 116
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_consultaFrances");
            echo "\">Francés</a>
              </li>
              <ul>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-empty\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"";
            // line 121
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_consultaFrancesBasico");
            echo "\" >Básico</a>
                </li>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-half\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"";
            // line 125
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_consultaFrancesIntermedio");
            echo "\" >Intermedio</a>
                </li>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-full\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"";
            // line 129
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_consultaFrancesAvanzado");
            echo "\" >Avanzado</a>
                </li>
              </ul>
            </ul>
         </li>

         <li class=\"nav-item has-treeview\">
           <a href=\"";
            // line 136
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_index");
            echo "\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-edit\"></i>
             <p>
               Inscripcion
               <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>
         ";
            // line 155
            echo "        </ul>
      ";
        }
        // line 157
        echo "
      ";
        // line 159
        echo "      ";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 160
            echo "        <li class=\"nav-item  has-treeview toc-entry toc-h2\">
           <a href=\"";
            // line 161
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_misgrupos");
            echo "\" class=\"nav-link\">
            <p><i class=\"nav-icon fa fa-group\"></i>
             Mis Grupos
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>
      ";
        }
        // line 169
        echo "
      ";
        // line 171
        echo "      ";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_RECEP")) {
            // line 172
            echo "        <li class=\"nav-item  has-treeview\">
           <a href=\"";
            // line 173
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_index");
            echo "\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-group\"></i>
             <p>Inscripcion
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>

      ";
        }
        // line 182
        echo "

     </nav>
   </div>
 </aside>


        ";
        // line 189
        $this->displayBlock('body', $context, $blocks);
        // line 190
        echo "
        <script>
  \$.widget.bridge('uibutton', \$.ui.button);
</script>


        <script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/datatables/dataTables.bootstrap4.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/jquery/jquery.slim.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/jQueryUI/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/bootstrap/js/bootstrap.bundle.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/select2/select2.full.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/input-mask/jquery.inputmask.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/input-mask/jquery.inputmask.date.extensions.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 204
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/input-mask/jquery.inputmask.extensions.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 205
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/colorpicker/bootstrap-colorpicker.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/timepicker/bootstrap-timepicker.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/timepicker/bootstrap-timepicker.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/slimScroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/iCheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/plugins/fastclick/fastclick.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/jquery/myScript.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/js/adminlte.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/js/pages/dashboard.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/js/demo.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.7/js/mdb.min.js"), "html", null, true);
        echo "\"></script>

        ";
        // line 220
        $this->displayBlock('javascripts', $context, $blocks);
        // line 221
        echo "    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "BeFluent";
    }

    // line 29
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 189
    public function block_body($context, array $blocks = array())
    {
    }

    // line 220
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  476 => 220,  471 => 189,  466 => 29,  460 => 5,  454 => 221,  452 => 220,  447 => 218,  443 => 217,  439 => 216,  435 => 215,  431 => 214,  427 => 213,  423 => 212,  419 => 211,  415 => 210,  411 => 209,  407 => 208,  403 => 207,  399 => 206,  395 => 205,  391 => 204,  387 => 203,  383 => 202,  379 => 201,  375 => 200,  371 => 199,  367 => 198,  363 => 197,  359 => 196,  351 => 190,  349 => 189,  340 => 182,  328 => 173,  325 => 172,  322 => 171,  319 => 169,  308 => 161,  305 => 160,  302 => 159,  299 => 157,  295 => 155,  284 => 136,  274 => 129,  267 => 125,  260 => 121,  252 => 116,  244 => 111,  237 => 107,  230 => 103,  222 => 98,  204 => 83,  197 => 79,  190 => 75,  179 => 67,  174 => 64,  171 => 63,  163 => 56,  157 => 54,  155 => 53,  150 => 52,  148 => 51,  143 => 50,  141 => 49,  137 => 48,  131 => 45,  118 => 35,  109 => 30,  107 => 29,  98 => 23,  94 => 22,  90 => 21,  86 => 20,  82 => 19,  78 => 18,  74 => 17,  70 => 16,  66 => 15,  62 => 14,  58 => 13,  54 => 12,  50 => 11,  46 => 10,  42 => 9,  38 => 8,  34 => 7,  29 => 5,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\base.html.twig");
    }
}
