<?php

/* :evaluacion:index.html.twig */
class __TwigTemplate_ebe3c100d9dad00d6fcbd9a20537161024e5cf34f732589e86c8725efbc166d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":evaluacion:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "   <div class=\"content-wrapper card\"> ";
        // line 5
        echo "<div class=\"containers\">
           <div class=\" p-3 mb-2 bg-color text-white\" >
                     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
             <center><h3 class=\"titulo\">Evaluaciones del grupo</h3></center>
             <div class=\"col-md-1  offset-md-11\">
               <a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_new");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/users-add-user-icon.png"), "html", null, true);
        echo "\" alt=\"new_user\" class=\"iconew\"></a>
             </div>
             </div>
           </div>
           <div class=\"card-body  table-responsive p-0\">
             <table class=\" table table-hover table-bordered\">
               <thead>
                   <tr>
                     <th>Nombre</th>
                     <th>Ponderacion</th>
                     <th>Grupo</th>

                   </tr>
               </thead>
               <tbody>
                ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["evaluacions"]) {
            // line 28
            echo "                   <tr>
                       <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "nombre", array()), "html", null, true);
            echo " </td>
                       <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "ponderacion", array()), "html", null, true);
            echo " </td>
                       <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "idGrupo", array()), "html", null, true);
            echo " </td>
                       <td>
                                <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_show", array("iddocente" => $this->getAttribute($context["evaluacions"], "id", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>
                                <a href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_edit", array("iddocente" => $this->getAttribute($context["evaluacions"], "id", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/edit.ico"), "html", null, true);
            echo "\" alt=\"Editar\" class=\"ico\"></a>
                       </td>
                   </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evaluacions'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "               </tbody>
             </table>
             <hr>
                <hr>
             <div class=\"navigation\">
                  ";
        // line 43
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : null));
        echo "
             </div>
           </div>
     </div><!-- /.row -->
   </div><!-- /.container-fluid -->
";
    }

    public function getTemplateName()
    {
        return ":evaluacion:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 43,  103 => 38,  91 => 34,  85 => 33,  80 => 31,  76 => 30,  72 => 29,  69 => 28,  65 => 27,  45 => 12,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":evaluacion:index.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/evaluacion/index.html.twig");
    }
}
