<?php

/* usuario/usuarioadmin.html.twig */
class __TwigTemplate_5de709b48739e610f3659f0bdf7af23f91b9df08f1b473b23898f0a82be038a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "usuario/usuarioadmin.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"content-wrapper card\"> ";
        // line 5
        echo "        <div class=\"containers\">
           <div class=\" p-3 mb-2 bg-color text-white\" >
                     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
             <center><h3 class=\"titulo\">Listado de Usuarios</h3></center>
             <div class=\"col-md-1  offset-md-11\">
               <a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_new");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/users-add-user-icon.png"), "html", null, true);
        echo "\" alt=\"new_user\" class=\"iconew\"></a>
             </div>
             </div>
           </div>

           <div class=\"card-body  table-responsive p-0\">
           Total records: ";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "getTotalItemCount", array()), "html", null, true);
        echo "

                <table class=\" table table-hover table-bordered\">
                    <thead>
                        <tr>
                            ";
        // line 24
        echo "                            <th>Username</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            ";
        // line 28
        echo "                            <th>Role</th>
                            ";
        // line 30
        echo "                            <th>Createdat</th>
                            <th>Updatedat</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["usuario"]) {
            // line 37
            echo "                        <tr>
                            ";
            // line 39
            echo "                            <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "username", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "nombre", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "email", array()), "html", null, true);
            echo "</td>
                            ";
            // line 42
            echo "                           
                            <td>
                                    ";
            // line 44
            if (($this->getAttribute($context["usuario"], "role", array()) == "ROLE_ADMIN")) {
                // line 45
                echo "                                        <strong>Administrador</strong></td>
                                    ";
            } elseif (($this->getAttribute(            // line 46
$context["usuario"], "role", array()) == "ROLE_USER")) {
                // line 47
                echo "                                        <strong>Profesor</strong>
                                    ";
            } elseif (($this->getAttribute(            // line 48
$context["usuario"], "role", array()) == "ROLE_RECEP")) {
                // line 49
                echo "                                        <strong>Recepcionista</strong>
                                    ";
            }
            // line 51
            echo "                            </td>

                            ";
            // line 54
            echo "
                            <td>";
            // line 55
            if ($this->getAttribute($context["usuario"], "createdAt", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["usuario"], "createdAt", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                            <td>";
            // line 56
            if ($this->getAttribute($context["usuario"], "updatedAt", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["usuario"], "updatedAt", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                            <td>
                                
                                <a href=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_show", array("id" => $this->getAttribute($context["usuario"], "id", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>
                                    
                                <a href=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_edit", array("id" => $this->getAttribute($context["usuario"], "id", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/edit.ico"), "html", null, true);
            echo "\" alt=\"Editar\" class=\"ico\"></a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usuario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "                    </tbody>
                </table>
                <div class=\"navigation\">
                    ";
        // line 68
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : null));
        echo "
                </div>
                ";
        // line 77
        echo "
            </div>
     </div><!-- /.row -->
   </div><!-- /.container-fluid -->
";
    }

    public function getTemplateName()
    {
        return "usuario/usuarioadmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 77,  165 => 68,  160 => 65,  148 => 61,  141 => 59,  133 => 56,  127 => 55,  124 => 54,  120 => 51,  116 => 49,  114 => 48,  111 => 47,  109 => 46,  106 => 45,  104 => 44,  100 => 42,  96 => 41,  92 => 40,  87 => 39,  84 => 37,  80 => 36,  72 => 30,  69 => 28,  64 => 24,  56 => 18,  45 => 12,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "usuario/usuarioadmin.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\usuario\\usuarioadmin.html.twig");
    }
}
