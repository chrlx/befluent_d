<?php

/* :security:login.html.twig */
class __TwigTemplate_5d4031694a5c13aa84bf39ba8d078d5bd3d571e331ab2d3cf98207dfeb79dae6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("principal.html.twig", ":security:login.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/css/signin.css"), "html", null, true);
        echo "\">
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "\t";
        $this->displayParentBlock("body", $context, $blocks);
        echo "

  <div class=\"cover\" style=\"background-image: url(";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/loginFont.jpg"), "html", null, true);
        echo ");\">
    <div class=\"container\">
  \t<form action=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("academiainscripcion_login_check");
        echo "\" method=\"post\"  class=\"full-box logInForm\">
  \t\t<p class=\"text-center text-muted\"><i class=\"zmdi zmdi-account-circle zmdi-hc-5x\"></i></p>
  \t\t<p class=\"text-center text-muted text-uppercase\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Inicia sesión con tu cuenta"), "html", null, true);
        echo "</p>
      ";
        // line 16
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 17
            echo "      <div class=\"text-danger\">
      </div>
      ";
        }
        // line 20
        echo "  \t\t<div class=\"form-group label-floating\">
  \t\t  <label class=\"control-label\" for=\"UserEmail\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Usuario"), "html", null, true);
        echo ":</label>
        <input type=\"text\" id=\"username\" class=\"form-control\" name=\"_username\" required autofocus />

  \t\t  <p class=\"help-block\">Escribe tú Usurio</p>
  \t\t</div>
  \t\t<div class=\"form-group label-floating\">
  \t\t  <label class=\"control-label\" for=\"UserPass\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Contraseña"), "html", null, true);
        echo "</label>
        <input type=\"password\" id=\"password\" class=\"form-control\" name=\"_password\" required />
  \t\t  <p class=\"help-block\">Escribe tú contraseña</p>
  \t\t</div>
  \t\t<div class=\"form-group text-center\">
        <input type=\"hidden\" name=\"_target_path\" value=\"academiainscripcion_homepage\" />
  \t\t\t<input type=\"submit\" value=\"Iniciar sesión\" class=\"btn btn-raised btn-danger\">
  \t\t</div>
  \t</form>

  </div>
";
    }

    public function getTemplateName()
    {
        return ":security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 27,  76 => 21,  73 => 20,  68 => 17,  66 => 16,  62 => 15,  57 => 13,  52 => 11,  46 => 9,  43 => 8,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":security:login.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/security/login.html.twig");
    }
}
