<?php

/* grupo/edit.html.twig */
class __TwigTemplate_a93f5cf1fff94f32691f2504bb748c0e1f1187e03a6a6cbb13fa0559942abb26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "grupo/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
  <div class=\"content-wrapper card\"> ";
        // line 6
        echo "  <div class=\"containers\">
        <div class=\"p-3 mb-2 bg-color text-white \">
    <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
      <div class=\"titulo\">
        <center><h1>Edición de Grupo</h1></center>
      </div>
      </div>
    <div class=\"card-body\">

      ";
        // line 17
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_start', array("attr" => array("role" => "form")));
        echo "
               <h3 class=\"card-title\">Datos De Grupo</h3>
               <br>
               <!-- text input -->
               <div class=\"row\">
                  <div class=\"col-sm-3 col-md-6 col-lg-6\">
                    <div class=\"input-group mb-3\">
                      <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
                      </div>
                      ";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "tipo", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Tipo de curso", "onpaste" => "return false")));
        echo "
                    </div>
                   </div>
                   <div class=\"col-sm-3 col-md-4 col-lg-6\">
                     <div class=\"bootstrap-timepicker\">

                     <div class=\"input-group-append\">
                        <label for=\"example-time-input\" class=\"col-3 col-form-label\">Hora Inico</label>
                       <span class=\"input-group-text\"><i class=\"fa fa-clock-o\"></i></span>

                         ";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "horario", array()), 'widget', array("attr" => array("type" => "time", "value" => "13:45:00", "id" => "example-time-input", "class" => "form-control")));
        echo "
                         <!--<input type=\"text\" class=\"form-control\" data-inputmask=\"'mask': ['9999-999999-999-9', '99999999-9' ]\" data-mask placeholder=\"DUI, menor de edad NIT\">-->

             <!-- <div class=\"md-form\">
  <input type=\"text\" id=\"manual-operations-input\" class=\"form-control\" placeholder=\"Now\">
  <label for=\"form1\" class=\"\">Check the minutes</label>
</div>

              <div class=\"md-form\">
  <input type=\"text\" class=\"form-control timepicker\" placeholder=\"Now\">
  <label for=\"form1\" class=\"\">Check the minutes</label>
</div>-->
                   </div>
                   <!-- /.input group -->
                 </div>
                 </div>
               </div>
               <div class=\"row\">
                 <div class=\"col-sm-3 col-md-4 col-lg-6\">
                   <div class=\"input-group mb-3\">
                     <div class=\"input-group-prepend\">
                       <span class=\"input-group-text\"><i class=\"fa fa-spinner\"></i></span>
                     </div>
                       ";
        // line 60
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "nivel", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Nivel (Basico, Avanzado, Intermedio)", "onpaste" => "return false")));
        echo "
                    <!-- <input type=\"text\" class=\"form-control\" data-inputmask=\"'alias': 'dd/mm/yyyy'\" data-mask placeholder=\"Fecha de nacimientos\">-->
                   </div>
                   </div>
                   <div class=\"col-sm-3 col-md-4 col-lg-6\">
                     <div class=\"bootstrap-timepicker\">
                       <div class=\"input-group-append\">
                          <label for=\"example-time-input\" class=\"col-3 col-form-label\">Hora Fin</label>
                         <span class=\"input-group-text\"><i class=\"fa fa-clock-o\"></i></span>

                           ";
        // line 70
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "horarioFin", array()), 'widget', array("attr" => array("type" => "time", "value" => "13:45:00", "id" => "example-time-input", "class" => "form-control")));
        echo "
                           <!--<input type=\"text\" class=\"form-control\" data-inputmask=\"'mask': ['9999-999999-999-9', '99999999-9' ]\" data-mask placeholder=\"DUI, menor de edad NIT\">-->
                     </div>
                   <!-- /.input group -->
                 </div>
                 </div>
               </div>
       <div class=\"row\">
          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
                <span class=\"input-group-text\"><i class=\"fa fa-group\"></i></span>
              </div>
              ";
        // line 83
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "salon", array()), 'widget', array("attr" => array("type" => "submit", "class" => "form-control", "placeholder" => "Salon")));
        echo "
              <!--<input type=\"email\" class=\"form-control\" placeholder=\"Email\">-->
            </div>
          </div>
          <div class=\"col-sm-3  col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
                  <span class=\"input-group-text\"><i class=\"fa fa-user-circle-o\"></i></span>
              </div>
                ";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "coach", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
          </div>
        </div>

    </div>
  </div>
</div>
<div class=\"content-wrapper card\"> ";
        // line 101
        echo "<div class=\"containers\">

      <div class=\"card-body\">
        <div class=\"row justify-content-between\">
          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <h1 class=\"card-title\">Modalidad</h1>
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
                <span class=\"input-group-text\"><i class=\"fa fa-maxcdn\"></i></span>
              </div>
                ";
        // line 111
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "modalidad", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Basico", "onpaste" => "return false")));
        echo "
             <!-- <input type=\"text\" class=\"form-control\" data-inputmask=\"'alias': 'dd/mm/yyyy'\" data-mask placeholder=\"Fecha de nacimientos\">-->
            </div>
            </div>
          </div>
        </div>
          </div>

</div>
<div class=\"content-wrapper card\">
  <div class=\"row justify-content-between\">
    <div class=\"col-md-2 offset-md-3\">
        <td>
          <button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button>
        </td>
      </div>

      <div class=\"col-md-2 \">
        <td>
          <a href=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_show", array("id" => $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "id", array()))), "html", null, true);
        echo "\"><button type=\"button\" class=\"btn btn-block btn-info fa fa-eye\">&nbsp;Cambios</button></a>
        </td>
      </div>

        <div class=\"col-md-2 \">
        <td>
          <a href=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("evaluacion_show", array("id" => $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "id", array()))), "html", null, true);
        echo "\"><button type=\"button\" class=\"btn btn-block btn-info fa fa-edit\">evaluar</button></a>
        </td>
      </div>


        <div class=\"col-md-2 \">
          <td>
            <a href=\"";
        // line 143
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_index");
        echo "\"><button type=\"button\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Cancelar</button></a>
          </td>
        </div>
    </div>
  </div>
              ";
        // line 148
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_end');
        echo " 
";
    }

    public function getTemplateName()
    {
        return "grupo/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 148,  209 => 143,  199 => 136,  190 => 130,  168 => 111,  156 => 101,  145 => 92,  133 => 83,  117 => 70,  104 => 60,  78 => 37,  65 => 27,  52 => 17,  41 => 9,  36 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "grupo/edit.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\grupo\\edit.html.twig");
    }
}
