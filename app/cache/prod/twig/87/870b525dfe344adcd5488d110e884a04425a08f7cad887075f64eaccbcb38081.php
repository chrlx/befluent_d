<?php

/* :docente:show.html.twig */
class __TwigTemplate_a51bb58c7d38619235dfa3646463a86a99589d6af6052c3948ff19d3fe649423 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":docente:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"content-wrapper card\"> ";
        // line 5
        echo "<div class=\"containers\">
  <div class=\"p-3 mb-2 bg-color text-white\">
           <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
    <h1 class=\"text-center\">Docente</h1>
  </div>
    <ul class=\"list-group\">
      <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DEL DOCENTE</b></li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NOMBRE:</b>        &nbsp;&nbsp;";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["docente"]) ? $context["docente"] : null), "nombre", array()), "html", null, true);
        echo "</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EDAD:</b> &nbsp;&nbsp;&nbsp;";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["docente"]) ? $context["docente"] : null), "edad", array()), "html", null, true);
        echo " años</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>DUI:</b> &nbsp;&nbsp;";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["docente"]) ? $context["docente"] : null), "dui", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TEL:</b> &nbsp;&nbsp;";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["docente"]) ? $context["docente"] : null), "telefono", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CORREO:</b> &nbsp;&nbsp;";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["docente"]) ? $context["docente"] : null), "email", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;<b>ESPECIALIZACION:</b> &nbsp;&nbsp;";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["docente"]) ? $context["docente"] : null), "especializacion", array()), "html", null, true);
        echo "</li>

</ul>
<div class=\"row justify-content-between\">
    <div class=\"col-md-2 offset-md-6\">
      <td>
        <br><br><br><br>
        <a href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_index");
        echo "\"><button type=\"submit\" class=\"btn btn-block btn-success fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
    </div>
    <div class=\"col-md-2 \">
      <td>
        <br><br><br><br>
      <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_edit", array("iddocente" => $this->getAttribute((isset($context["docente"]) ? $context["docente"] : null), "iddocente", array()))), "html", null, true);
        echo " \"  class=\"btn btn-block btn-info fa fa-edit\">&nbsp; Editar</a>
    </td>
    </div>
    <div class=\"col-lg-2\">
      <br><br><br><br>
      ";
        // line 37
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
        echo "
          <button type=\"submit\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Eliminar</button>
      ";
        // line 39
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
        echo "
    </div>
  </div>
  <br>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return ":docente:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 39,  94 => 37,  86 => 32,  77 => 26,  67 => 19,  63 => 18,  59 => 17,  55 => 16,  51 => 15,  47 => 14,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":docente:show.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/docente/show.html.twig");
    }
}
