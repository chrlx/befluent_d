<?php

/* evaluacion/new.html.twig */
class __TwigTemplate_e5b3e5bea84fbc7c726de65cb875335e92d2a580dedebda3b4a1f9523f96a87b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "evaluacion/new.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    

<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>

<script type=\"text/javascript\" >

var nombres=[];
var ponderaciones=[];
var i=0;
var k=0;
var tamano = 0;

function agregarEvaluacion(){
            
            var _nombre = document.getElementById(\"academia_inscripcionbundle_evaluacion_nombre\").value;
            var _ponderacion = document.getElementById(\"academia_inscripcionbundle_evaluacion_ponderacion\").value;
            nombres[i]=_nombre;
            ponderaciones[i]=_ponderacion;
            switch(nombres.length){
              case 1:
                var contenido=_nombre;
                \$('#u1').html(_nombre);
                \$('#d1').html(_ponderacion);
                tamano = nombres.length;
                break;

               case 2:
                var contenido=_nombre;
                \$('#u2').html(_nombre);
                \$('#d2').html(_ponderacion);
                tamano = nombres.length;
                break;
              case 3:
                \$('#u3').html(_nombre);
                \$('#d3').html(_ponderacion);
                tamano = nombres.length;
                break;
              case 4:
                var contenido=_nombre;
                \$('#u4').html(_nombre);
                \$('#d4').html(_ponderacion);
                tamano = nombres.length;
                break;
              case 5:
                \$('#u5').html(_nombre);
                \$('#d5').html(_ponderacion);
                tamano = nombres.length;
                break;
                case 6:
                var contenido=_nombre;
                \$('#u6').html(_nombre);
                \$('#d6').html(_ponderacion);
                tamano = nombres.length;
                break;
                case 7:
                \$('#u7').html(_nombre);
                \$('#d7').html(_ponderacion);
                tamano = nombres.length;
                break;
                case 8:
                \$('#u8').html(_nombre);
                \$('#d8').html(_ponderacion);
                tamano = nombres.length;
                break;
                case 9:
                \$('#u9').html(_nombre);
                \$('#d9').html(_ponderacion);
                tamano = nombres.length;
                break;
                case 10:
                \$('#u10').html(_nombre);
                \$('#d10').html(_ponderacion);
                tamano = nombres.length;
                break;
                case 11:
                alert(\"Deben ser un maximo de 10 evaluaciones\")
                break;
            }
            
           /* var fila=\"<tr><td id=a>\"+_nombre+\"</td><td>\"+_ponderacion+\"</td></tr>\";

            var btn = document.createElement(\"TR\");
            btn.innerHTML=fila;
            document.getElementById(\"filas\").appendChild(btn);*/
            i++;
            return false;
            
           
            
}


    function guardarEvaluacion(){
            //var jsonNombres= JSON.stringify(nombres);
            //var jsonPonderaciones = JSON.stringify(ponderaciones);
            var grupo = document.getElementById(\"academia_inscripcionbundle_evaluacion_idGrupo\").value;
            var idGrupoInt=parseInt(grupo);
            var totalPonderaciones=0;
            for (var i = 0; i < ponderaciones.length; i++) {
            
              totalPonderaciones=totalPonderaciones+parseInt(ponderaciones[i]);
            
        }
        if (totalPonderaciones==100) {
              
            
            \$.ajax({
                data : {'nombre':nombres , 'ponderacion':ponderaciones,'tam':tamano,'grupo':idGrupoInt},
                url: \"";
        // line 112
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("evaluacion_new");
        echo "\",
                type: 'POST',
            
                success: function(data){
                   alert(\"Guardado\");
                  }
                
        
            });
          }else{
            alert('La suma total de las evaluaciones debe ser igual a 100');
          }
              
        }
          \$('#guardar').onclick(obtenerEvaluacion());
          \$('#s').onclick(agregarEvaluacion());


          function eliminarEvaluacion(){
             switch(nombres.length){
               case 0:

               break
              case 1:
                
                \$('#u1').html(\"--\");
                \$('#d1').html(\"--\");
                nombres.pop();
               ponderaciones.pop();
               i--;
                break;

               case 2:
                 
                \$('#u2').html(\"--\");
                \$('#d2').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                 i--;
                break;
              case 3:
                 
                \$('#u3').html(\"--\");
                \$('#d3').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                i--;
                break;
              case 4:
                 
                \$('#u4').html(\"--\");
                \$('#d4').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                i--;
                break;
              case 5:
                 
                \$('#u5').html(\"--\");
                \$('#d5').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                i--;
                break;
                case 6:
                 
                \$('#u6').html(\"--\");
                \$('#d6').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                i--;
                break;
                case 7:
                  
                \$('#u7').html(\"--\");
                \$('#d7').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                i--;
                break;
                case 8:
                 
                \$('#u8').html(\"--\");
                \$('#d8').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                i--;
                break;
                case 9:
                 
                \$('#u9').html(\"--\");
                \$('#d9').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                i--;
                break;
                case 10:
                 
                \$('#u10').html(\"--\");
                \$('#d10').html(\"--\");
               nombres.pop();
               ponderaciones.pop();
                i--;
                break;
               
            }
          }




</script>



";
    }

    // line 229
    public function block_body($context, array $blocks = array())
    {
        // line 230
        echo "
<div class=\"content-wrapper card\"> ";
        // line 232
        echo "<div class=\"containers\">
      <div class=\"p-3 mb-2 bg-color text-white\">
         <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 235
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
      <center><h1 class=\"titulo\">Evaluacion</h1></center>
    </div>
    <div class=\"card-body\">
      ";
        // line 240
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("role" => "form")));
        echo "
               <h3 class=\"card-title\">Información de Evaluacion</h3>
               <br>
               <div class=\"row\">
                  <div class=\"col-sm-3 col-md-6 col-lg-12\">
                    <div class=\"input-group mb-3\">
                      <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
                      </div>
                      <!--";
        // line 249
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array()), 'widget', array("attr" => array("type" => "text", "class" => "form-control", "placeholder" => "Nombre Completo", "onpaste" => "return false", "required" => "true")));
        echo "-->

                      <input type=\"text\" name=\"nombre\" class=\"form-control \" required=\"true\" id=\"academia_inscripcionbundle_evaluacion_nombre\" placeholder='Nombre de la evaluacion'>
                     <!-- text input <input type=\"text\" onkeypress=\"return soloLetras(event)\" onpaste=\"return false\" class=\"form-control\" placeholder=\"Nombres\">-->
                    </div>
                   </div>
               </div>
               <div class=\"row\">
                 <div class=\"col-sm-3 col-md-4 col-lg-6\">
                   <div class=\"input-group mb-3\">
                     <div class=\"input-group-prepend\">
                       <span class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></span>
                     </div>
                       ";
        // line 262
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ponderacion", array()), 'widget', array("attr" => array("type" => "number", "class" => "form-control", "onkeypress" => "return isNumberKey(event)", "placeholder" => "Ponderacion", "onpaste" => "return false", "max" => "100", "onpaste" => "return false")));
        echo "
                   </div>
                   </div>
                 
                          
                          <!--<input type=\"text\" class=\"form-control\" data-inputmask=\"'mask': ['9999-999999-999-9', '99999999-9' ]\" data-mask placeholder=\"DUI, menor de edad NIT\">-->
                        </div>
                         <div class=\"col-lg-4\">
                          <h3 class=\"card-title\">Horarios</h3>
                          <br>
                           ";
        // line 272
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "idGrupo", array()), 'widget', array("attr" => array("class" => "form-control ")));
        echo "
            </div>
                 
               </div>
       
        <hr>
        <br>
        <div class=\"row justify-content-center\">
          <table border id=\"tablaE\">
            <thead>
              <th width=\"145\">Evaluaciones</th>
              
            </thead>
            <tbody id=\"filas\" class=\"text-center\">
              <tr>
                <td >Nombre</td>
                <td id=\"u1\" width=\"80\"> -- </td>
                <td id=\"u2\" width=\"80\"> -- </td>
                <td id=\"u3\" width=\"80\"> -- </td>
                <td id=\"u4\" width=\"80\"> -- </td>
                <td id=\"u5\" width=\"80\"> -- </td>
                <td id=\"u6\" width=\"80\"> -- </td>
                <td id=\"u7\" width=\"80\"> -- </td>
                <td id=\"u8\" width=\"80\"> -- </td>
                <td id=\"u9\" width=\"80\"> -- </td>
                <td id=\"u10\" width=\"80\"> -- </td>
                
              </tr>
              <tr>
                <td >Ponderacion</td>
                <td id=\"d1\" width=\"80\"> -- </td>
                <td id=\"d2\" width=\"80\"> -- </td>
                <td id=\"d3\" width=\"80\"> -- </td>
                <td id=\"d4\" width=\"80\"> -- </td>
                <td id=\"d5\" width=\"80\"> -- </td>
                <td id=\"d6\" width=\"80\"> -- </td>
                <td id=\"d7\" width=\"80\"> -- </td>
                <td id=\"d8\" width=\"80\"> -- </td>
                <td id=\"d9\" width=\"80\"> -- </td>
                <td id=\"d10\" width=\"80\"> -- </td>
              </tr>

            </tbody>
          </table>  
         
        </div>
        
        <hr>
        <br>

          <div class=\"row justify-content-between\">
              <div class=\"col-md-4\"></div>
              <div class=\"col-md-2\">
                <button type=\"button\" class=\"btn btn-block btn-success fa fa-plus\" id=\"prueba \" onclick=\"agregarEvaluacion();\">&nbsp;Agregar</button>
              </div>
              <div class=\"col-md-2\">
                <button class=\"btn btn-block btn-info fa fa-save\" id=\"agregar \" onclick=\"guardarEvaluacion();\">&nbsp;Guardar</button>
              </div>
              <div class=\"col-md-2\">
                <button class=\"btn btn-block btn-danger fa fa-trash\" id=\"eliminarEvaluacion \" onclick=\"eliminarEvaluacion();\">&nbsp;Eliminar</button>
              </div>
              <div class=\"col-md-2\">
                <a href=\"";
        // line 334
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_index");
        echo "\"><button type=\"button\" class=\"btn btn-block btn-secundary fa fa-close\">&nbsp;Regresar</button></a>
              </div>
                ";
        // line 336
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
         </div>
    </div>
  </div>

";
    }

    public function getTemplateName()
    {
        return "evaluacion/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  392 => 336,  387 => 334,  322 => 272,  309 => 262,  293 => 249,  281 => 240,  273 => 235,  268 => 232,  265 => 230,  262 => 229,  142 => 112,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "evaluacion/new.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\evaluacion\\new.html.twig");
    }
}
