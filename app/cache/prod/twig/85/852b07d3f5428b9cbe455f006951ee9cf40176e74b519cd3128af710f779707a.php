<?php

/* :usuario:home.html.twig */
class __TwigTemplate_c43e1a1eedcc0fb562deb3414b72acd36a5fa8eb23c2ed4140f9184e89fe727f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("principal.html.twig", ":usuario:home.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
\t<div class=\"container\">
\t\t<nav class=\"navbar navbar-light navbar-expand-md navigation-clean-button\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<h1 class=\"text-center\">Befluent - Bienvenido</h1>
\t\t\t\t\t<h3 class=\"text-center\">
\t\t\t\t\t\t";
        // line 10
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_ADMIN")) {
            // line 11
            echo "\t\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Administrador"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        } elseif (($this->getAttribute($this->getAttribute(        // line 12
(isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_USER")) {
            // line 13
            echo "\t\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Profesor"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        } elseif (($this->getAttribute($this->getAttribute(        // line 14
(isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_RECEP")) {
            // line 15
            echo "    \t";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Recepcionista"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        }
        // line 17
        echo "\t\t\t\t\t\t\t :\t";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo "
\t\t\t\t\t\t</h3>
\t\t\t\t</div>\t

\t\t\t<div class=\"col-xs-11 col-md-11\"></div>
\t\t\t\t<div class=\"col-xs-1 col-md-1\">
\t\t\t\t\t<a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("academiainscripcion_logout");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/logout.png"), "html", null, true);
        echo "\" alt=\"logout\" title=\"logout\" class=\"iconew\"></a>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br> 
             </div>
\t\t</nav>

\t</div>
\t<div class=\"container\">
\t\t<div  class=\"full-box text-center\" style=\"padding: 30px 10px;\">
\t\t\t";
        // line 33
        echo "     \t\t";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 34
            echo "\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Asistencia</div>
\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t<i class=\"fa fa-line-chart fa-8x\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"";
            // line 49
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_index");
            echo "\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Grupos</div>
\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t<i class=\"fa fa-list-ul fa-10x\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"";
            // line 64
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
            echo "\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Usuarios</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-slideshare fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t";
            // line 73
            echo "\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"";
            // line 80
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_index");
            echo "\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Studiantes</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-weixin fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t";
        }
        // line 94
        echo "\t\t\t
\t\t\t";
        // line 96
        echo "\t\t\t ";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 97
            echo "\t\t\t <div class=\"col-md-4\"></div>
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t<a href=\"";
            // line 99
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_misgrupos");
            echo "\">
\t\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Grupos Asignados</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-list-ul fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        }
        // line 114
        echo "
\t\t\t";
        // line 116
        echo "     \t\t";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_RECEP")) {
            // line 117
            echo "
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"";
            // line 119
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_index");
            echo "\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Inscripciones</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-weixin fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<small>Nueva Inscripcion</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t";
        }
        // line 132
        echo "\t\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return ":usuario:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 132,  200 => 119,  196 => 117,  193 => 116,  190 => 114,  172 => 99,  168 => 97,  165 => 96,  162 => 94,  145 => 80,  136 => 73,  125 => 64,  107 => 49,  90 => 34,  87 => 33,  73 => 23,  63 => 17,  57 => 15,  55 => 14,  50 => 13,  48 => 12,  43 => 11,  41 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":usuario:home.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/usuario/home.html.twig");
    }
}
