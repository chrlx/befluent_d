<?php

/* :evaluacion:edit.html.twig */
class __TwigTemplate_aa8da6789475c3ac87bcdd290de62135142ed14a73047e4fea5984c30e575135 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":evaluacion:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"content-wrapper card\"> ";
        // line 5
        echo "<div class=\"containers\">
           <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
    <h1 class=\"text-center  p-3 mb-2 bg-color text-white\">EVALUACION</h1>
    <ul class=\"list-group\">
      <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DE LA EVALUACION</b></li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NOMBRE:</b>        &nbsp;&nbsp;";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["evaluacion"]) ? $context["evaluacion"] : null), "nombre", array()), "html", null, true);
        echo "</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EVALUACION:</b> &nbsp;&nbsp;&nbsp;";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["evaluacion"]) ? $context["evaluacion"] : null), "ponderacion", array()), "html", null, true);
        echo " </li>
   

</ul>
<div class=\"row justify-content-between\">
    <div class=\"col-md-2 offset-md-6\">
      <td>
        <br><br><br><br>
         <a href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("evaluacion_show", array("id" => $this->getAttribute((isset($context["grupoDeEvaluacion"]) ? $context["grupoDeEvaluacion"] : null), "id", array()))), "html", null, true);
        echo "\"><button type=\"submit\" class=\"btn btn-block btn-success fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
    </div>
    <div class=\"col-md-2 \">
      <td>
        <br><br><br><br>
    </td>
    </div>
    <div class=\"col-lg-2\">
      <br><br><br><br>
      ";
        // line 31
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
        echo "
          <button type=\"submit\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Eliminar</button>
      ";
        // line 33
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
        echo "
    </div>
  </div>
  <br>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return ":evaluacion:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 33,  73 => 31,  60 => 21,  49 => 13,  45 => 12,  37 => 7,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":evaluacion:edit.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/evaluacion/edit.html.twig");
    }
}
