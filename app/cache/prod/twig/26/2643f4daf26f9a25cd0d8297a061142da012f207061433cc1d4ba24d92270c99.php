<?php

/* usuario/edit.html.twig */
class __TwigTemplate_eb79913c11af8fc17fa3d4ebd152e0c1961ec6e30d4f7052c4e77405440a5a78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "usuario/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"content-wrapper card\"> ";
        // line 5
        echo "    <div class=\"containers\">
          <div class=\" p-3 mb-2 bg-color text-white \">
                     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
          <center><h3 class=\"titulo\"><strong>Edicion de Usuario</strong></h3></center>
        </div>
        <div class=\"card-body\">
          ";
        // line 13
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_start', array("attr" => array("role" => "form")));
        echo "
                   <h3 class=\"card-title\"><strong>Información Personal</strong> </h3>
                   <br>

                   <div class=\"row\">
                      <div class=\"col-sm-12 col-md-12 col-lg-12\">
                        <div class=\"input-group mb-3\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
                          </div>
                          ";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "nombre", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Nombre Completo", "onpaste" => "return false")));
        echo "
                          <!--, 'disabled':''-->
                        </div>
                       </div>
                   </div>
                   <div class=\"row\">
                     <div class=\"col-sm-6 col-md-6 col-lg-6\">
                       <div class=\"input-group mb-3\">
                         <div class=\"input-group-prepend\">
                           <span class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></span>
                         </div>
                           ";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "username", array()), 'widget', array("attr" => array("type" => "text", "class" => "form-control", "placeholder" => "username", "onpaste" => "return false", "min" => "18", "max" => "100", "onpaste" => "return false")));
        echo "
                       </div>
                       </div>
                       <div class=\"col-sm-6 col-md-6 col-lg-6\">
                         <div class=\"input-group mb-3\">
                              <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\"><i class=\"fa fa-address-card-o\"></i></span>
                              </div>
                              ";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "password", array()), 'widget', array("attr" => array("type" => "text", "class" => "form-control", "placeholder" => "password", "onpaste" => "return false")));
        echo "
                            </div>
                     </div>
                   </div>
                       <div class=\"row\">
                          <div class=\"col-sm-6 col-md-6 col-lg-6\">
                            <div class=\"input-group mb-3\">
                              <div class=\"input-group-prepend\">
                                <span class=\"input-group-text\"><i class=\"fa fa-envelope\"></i></span>
                              </div>
                              ";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "email", array()), 'widget', array("attr" => array("type" => "email", "class" => "form-control", "placeholder" => "Email")));
        echo "
                            </div>
                          </div>
                          <div class=\"col-sm-6  col-md-6 col-lg-6\">
                            <div class=\"input-group mb-3\">
                                 <div class=\"input-group-prepend\">
                                   <span class=\"input-group-text\"><i class=\"fa fa-phone\"></i></span>
                                 </div>
                                 ";
        // line 60
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "role", array()), 'widget', array("attr" => array("type" => "text", "data-mask" => "", "class" => "form-control", "onpaste" => "return false")));
        echo "
                               </div>
                          </div>
                        </div>
                        <div class= \"checkbox minimal-red\">
                            <label>
                                ";
        // line 66
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "isActive", array()), 'widget');
        echo "Activo
                                <span class=\"text-danger\">";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "isActive", array()), 'errors');
        echo "</span>
                            </label>
                        </div>
                      </div>
                    </div>
                  </div>
    <div class=\"content-wrapper card\">
      <div class=\"containers\">
          <div class=\"card-body\">
            <div class=\"row justify-content-between \">
                <div class=\"col-md-2 offset-md-8\">
                  <a href=\"";
        // line 78
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
        echo "\"><button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button></a>
                  <br>
                </div>
                <div class=\"col-md-1 col-lg-2 \">
                  <a href=\"";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
        echo "\"><button type=\"button\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Cancelar</button></a>
                  <br>
                </div>
                  ";
        // line 85
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_end');
        echo "
              </div>
             </div>
           </div>
      </div>

";
    }

    public function getTemplateName()
    {
        return "usuario/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 85,  142 => 82,  135 => 78,  121 => 67,  117 => 66,  108 => 60,  97 => 52,  84 => 42,  73 => 34,  59 => 23,  46 => 13,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "usuario/edit.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\usuario\\edit.html.twig");
    }
}
