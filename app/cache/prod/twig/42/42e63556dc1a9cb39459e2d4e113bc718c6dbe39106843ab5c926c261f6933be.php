<?php

/* usuario/new.html.twig */
class __TwigTemplate_f0f6870d01f60df5377c545cd5f2e613b1ad87acdac0df1fd6d1b12b597186f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "usuario/new.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
    <div class=\"content-wrapper card\">
        <div class=\"container\">
              <div class=\" p-3 mb-2 bg-color text-white \">
                         <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
                <div class=\"card-body \">
                  <center><h1 class=\"titulo\">Creación de Usuario</h1></center>
                </div>
              </div>

                    ";
        // line 20
        echo "                    <div class=\"card-body\">
                    ";
        // line 21
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("novalidate" => "novalidate", "role" => "form")));
        echo "
                    <br>
                    <div class=\"row\">
                       <div class=\"col-sm-3 col-md-6 col-lg-12\">
                         <div class=\"input-group mb-3\">
                           <div class=\"input-group-prepend\">
                             <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
                           </div>
                           ";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "username", array()), 'widget', array("attr" => array("type" => "text", "class" => "form-control", "placeholder" => "UserName", "onpaste" => "return false")));
        echo "
                          <!-- text input <input type=\"text\" onkeypress=\"return soloLetras(event)\" onpaste=\"return false\" class=\"form-control\" placeholder=\"Nombres\">-->
                            <span class=\"text-danger\">";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "username", array()), 'errors');
        echo "</span>
                         </div>
                        </div>
                    </div>
                    <div class=\"row\">
                      <div class=\"col-sm-3 col-md-4 col-lg-6\">
                        <div class=\"input-group mb-3\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></span>
                          </div>
                            ";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Nombre", "onpaste" => "return false")));
        echo "
                         <!-- <input type=\"text\" class=\"form-control\" data-inputmask=\"'alias': 'dd/mm/yyyy'\" data-mask placeholder=\"Fecha de nacimientos\">-->
                         <span class=\"text-danger\">";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array()), 'errors');
        echo "</span>
                        </div>
                        </div>
                        <div class=\"col-sm-3 col-md-4 col-lg-6\">
                          <div class=\"input-group mb-3\">
                            <div class=\"input-group-prepend\">
                              <span class=\"input-group-text\"><i class=\"fa fa-envelope\"></i></span>
                            </div>
                            ";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("type" => "email", "class" => "form-control", "placeholder" => "Email")));
        echo "
                            <!--<input type=\"email\" class=\"form-control\" placeholder=\"Email\">-->
                            <span class=\"text-danger\">";
        // line 53
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
        echo "</span>
                          </div>
                        </div>
                    </div>
                    <div class=\"row\">
                      <div class=\"col-sm-3 col-md-4 col-lg-6\">
                        <div class=\"input-group mb-3\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></span>
                          </div>
                            ";
        // line 63
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'widget', array("attr" => array("type" => "password", "id" => "password", "class" => "form-control", "placeholder" => "password", "onpaste" => "return false", "" => "required")));
        echo "
                         <!-- <input type=\"text\" class=\"form-control\" data-inputmask=\"'alias': 'dd/mm/yyyy'\" data-mask placeholder=\"Fecha de nacimientos\">-->
                         <span class=\"text-danger\">";
        // line 65
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'errors');
        echo "</span>
                        </div>
                        </div>
                        <div class=\"col-sm-3 col-md-4 col-lg-6\">
                          <div class=\"input-group mb-3\">
                            <div class=\"input-group-prepend\">
                              <span class=\"input-group-text\"><i class=\"fa fa-envelope\"></i></span>
                            </div>
                            ";
        // line 73
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "role", array()), 'widget', array("attr" => array("type" => "text", "class" => "form-control")));
        echo "
                            <!--<input type=\"email\" class=\"form-control\" placeholder=\"Email\">-->
                            <span class=\"text-danger\">";
        // line 75
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "role", array()), 'errors');
        echo "</span>
                          </div>
                        </div>
                    </div>
                    <div class= \"checkbox\">
                        <label>
                            ";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "isActive", array()), 'widget');
        echo "
                            ";
        // line 82
        if ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "isActive", array())) {
            echo "Activo";
        } else {
            echo "Inactivo";
        }
        // line 83
        echo "                            <span class=\"text-danger\">";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "isActive", array()), 'errors');
        echo "</span>
                        </label>
                    </div>
                    </div>
                    </div>
                    </div>

                    <div class=\"content-wrapper card\">
                      <div class=\"containers\">
                          <div class=\"card-body\">

                              <div class=\"row justify-content-between\">

                                  <div class=\"col-md-2 offset-md-8\">
                                      <button type=\"submit\" class=\"btn btn-block btn-success fa fa-check\">&nbsp;Acceptar</button>
                                  </div>
                                  <div class=\"col-lg-2 \">
                                      <a href=\"";
        // line 100
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
        echo "\"><button type=\"button\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp;Regresar</button></a>
                                  </div>
                                  ";
        // line 102
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "

                              </div>
                             </div>
                         </div>
                         ";
        // line 107
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
                      </div>
";
    }

    public function getTemplateName()
    {
        return "usuario/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 107,  180 => 102,  175 => 100,  154 => 83,  148 => 82,  144 => 81,  135 => 75,  130 => 73,  119 => 65,  114 => 63,  101 => 53,  96 => 51,  85 => 43,  80 => 41,  67 => 31,  62 => 29,  51 => 21,  48 => 20,  38 => 12,  31 => 7,  28 => 6,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "usuario/new.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\usuario\\new.html.twig");
    }
}
