<?php

/* estudiante/show.html.twig */
class __TwigTemplate_782488dda2de6226a4e05a962e06098f82b54bc247e2b6fa9b956b6522e5db8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "estudiante/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"content-wrapper card\"> ";
        // line 5
        echo "<div class=\"containers\">
  <div class=\"p-3 mb-2 bg-color text-white\">
             <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
    <h1 class=\"text-center titulo\">Estudiante</h1>
      </div>
      <div class=\"card-body\">
       <ul class=\"list-group\">
        <li class=\"list-group-item\"> <b> INFORMACION DEL ESTUDIANTE</b></li>
  <li class=\"list-group-item\"><b>NOMBRE:</b>&nbsp;&nbsp;";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "nombre", array()), "html", null, true);
        echo "</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EDAD:</b> &nbsp;&nbsp;";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "edad", array()), "html", null, true);
        echo " años</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>DUI:</b> &nbsp;&nbsp;";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "dui", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TEL:</b> &nbsp;&nbsp;";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "telefono", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;<b>CORREO:</b> &nbsp;&nbsp;";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "email", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DEL CURSO</b></li>
  <li class=\"list-group-item\">
      ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "grupos", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
            // line 23
            echo "      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TIPO:</b>&nbsp;&nbsp; ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "tipo", array()), "html", null, true);
            echo "
      <br>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "      </li>
  <li class=\"list-group-item\">
      ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "grupos", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
            // line 29
            echo "      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NIVEL:</b> &nbsp;&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "Nivel", array()), "html", null, true);
            echo "
      <br>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "      </li>
  <li class=\"list-group-item\">
      ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "grupos", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
            // line 35
            echo "      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SALON:</b>&nbsp;&nbsp; ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "salon", array()), "html", null, true);
            echo "
      <br>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "      </li>
   <li class=\"list-group-item\">
      ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "grupos", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
            // line 41
            echo "      <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INICIA:</b> &nbsp;&nbsp;";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["grupo"], "horario", array()), " H:i:s"), "html", null, true);
            echo "
      <br>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "      </li>
      <li class=\"list-group-item\">
         ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "grupos", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
            // line 47
            echo "         <b>FINALIZA:</b> &nbsp;&nbsp;";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["grupo"], "horarioFin", array()), " H:i:s"), "html", null, true);
            echo "
         <br>
         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "         </li>
</ul>
<br>
<hr>
<div class=\"row justify-content-between\">
    <div class=\"col-md-2 offset-md-6\">
      <td>
        <br><br><br><br>
        <a href=\"";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_index");
        echo "\"><button type=\"submit\" class=\"btn btn-block btn-primary  fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
    </div>
    <div class=\"col-md-2 \">
      <td>
        <br><br><br><br>
      <a href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_edit", array("id" => $this->getAttribute((isset($context["estudiante"]) ? $context["estudiante"] : null), "id", array()))), "html", null, true);
        echo " \"  class=\"btn btn-block btn-info fa fa-edit\">&nbsp; Editar</a>
    </td>
    </div>
    <div class=\"col-lg-2\">
      <br><br><br><br>
      ";
        // line 69
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
        echo "
          <button type=\"submit\"  class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Eliminar</button>
      ";
        // line 71
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
        echo "
    </div>
  </div>
  <br>
  </div>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "estudiante/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 71,  183 => 69,  175 => 64,  166 => 58,  156 => 50,  146 => 47,  142 => 46,  138 => 44,  128 => 41,  124 => 40,  120 => 38,  110 => 35,  106 => 34,  102 => 32,  92 => 29,  88 => 28,  84 => 26,  74 => 23,  70 => 22,  64 => 19,  60 => 18,  56 => 17,  52 => 16,  48 => 15,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "estudiante/show.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\estudiante\\show.html.twig");
    }
}
