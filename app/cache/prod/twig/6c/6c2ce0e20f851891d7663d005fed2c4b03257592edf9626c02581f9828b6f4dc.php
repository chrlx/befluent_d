<?php

/* :estudiante:new.html.twig */
class __TwigTemplate_ef8ec0cb1339673ecb842e0d9edfc949944971dd989347886cbc9899599bc027 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":estudiante:new.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>

<script type=\"text/javascript\">
    function obtenerGrupos(){
            var id = document.getElementById(\"cursos\").value;
            \$.ajax({
                data : {'id':id},
                url: \"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("estudiante_ajax");
        echo "\",
                type: 'POST',
                dataType:'json',
                success: function(data){
                    var html = \"\"
                    for(var i = 0; i<data.length ; i++){
                    html += '<option value=\"'+data[i].ids+'\">'+data[i].hora+' - '+ data[i].horaFin+' - ' +data[i].modalidad+'</option>'
                }
                    \$('#academia_inscripcionbundle_estudiante_grupos').html(html);
                }

            });
        }
          \$('#cursos').onchange(obtenerGrupos());



</script>

";
    }

    // line 32
    public function block_body($context, array $blocks = array())
    {
        // line 33
        echo "<div class=\"content-wrapper card\"> ";
        // line 34
        echo "<div class=\"containers\">

    <!-- /.card-header -->
      <div class=\" p-3 mb-2 bg-color text-white \">
                 <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
    <div class=\"card-body \">
      <center><h1 class=\"titulo\">Inscripción de Estudiante</h1></center>
    </div>
    </div>
    <div class=\"card-body\">
      ";
        // line 46
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("role" => "form")));
        echo "
               <h3 class=\"card-title\">Informacion Personal</h3>
               <br>
               <!-- text input -->
               <div class=\"row\">
                  <div class=\"col-sm-3 col-md-6 col-lg-12\">
                    <div class=\"input-group mb-3\">
                      <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
                      </div>
                      ";
        // line 56
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Nombre Completo", "onpaste" => "return false")));
        echo "
                     <!-- text input <input type=\"text\" onkeypress=\"return soloLetras(event)\" onpaste=\"return false\" class=\"form-control\" placeholder=\"Nombres\">-->
                    </div>
                   </div>
               </div>
               <div class=\"row\">
                 <div class=\"col-sm-3 col-md-4 col-lg-6\">
                   <div class=\"input-group mb-3\">
                     <div class=\"input-group-prepend\">
                       <span class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></span>
                     </div>
                     ";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edad", array()), 'widget', array("attr" => array("type" => "number", "class" => "form-control", "onkeypress" => "return isNumberKey(event)", "placeholder" => "Edad", "min" => "10", "max" => "100", "onpaste" => "return false")));
        echo "
                    <!-- <input type=\"text\" class=\"form-control\" data-inputmask=\"'alias': 'dd/mm/yyyy'\" data-mask placeholder=\"Fecha de nacimientos\">-->
                   </div>
                   </div>
                   <div class=\"col-sm-3 col-md-4 col-lg-6\">
                     <div class=\"input-group mb-3\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-address-card\"></i></span>
                          </div>
                          ";
        // line 76
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dui", array()), 'widget', array("attr" => array("type" => "text", "data-inputmask" => "'mask' : ['99999999-9', '9999-999999-999-9']", "data-mask" => "", "class" => "form-control", "placeholder" => "DUI, menor de edad NIT (Agregue los guiones)", "onpaste" => "return false")));
        echo "
                          <!--<input type=\"text\" class=\"form-control\" data-inputmask=\"'mask': ['9999-999999-999-9', '99999999-9' ]\" data-mask placeholder=\"DUI, menor de edad NIT\">-->
                        </div>
                 </div>
               </div>
       <div class=\"row\">
          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
                <span class=\"input-group-text\"><i class=\"fa fa-envelope\"></i></span>
              </div>
              ";
        // line 87
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("type" => "email", "class" => "form-control", "placeholder" => "Email")));
        echo "
              <!--<input type=\"email\" class=\"form-control\" placeholder=\"Email\">-->
            </div>
          </div>
          <div class=\"col-sm-3  col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
                 <div class=\"input-group-prepend\">
                   <span class=\"input-group-text\"><i class=\"fa fa-phone\"></i></span>
                 </div>
                 ";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "telefono", array()), 'widget', array("attr" => array("type" => "text", "data-inputmask" => "'mask': ['99999999', '9999-9999', '(+999) 9999-9999']", "data-mask" => "", "class" => "form-control", "placeholder" => "Telefono (agregar extencion antepone un +)", "onpaste" => "return false")));
        echo "
                 <!--<input type=\"text\" class=\"form-control\"  data-inputmask=\"'mask': [ '(+999) 9999-9999','9999-9999']\" data-mask placeholder=\"Numero de contacto\">-->
               </div>
          </div>
        </div>

    </div>
</div>
</div>
<div class=\"content-wrapper card\">
  <div class=\"containers\">
      <div class=\"card-body\">
      <div class=\"row\">
         <div class=\"col-lg-4\">
                <h3 class=\"card-title\">Cursos</h3>
                <br>
                    <select class=\"form-control\" id=\"cursos\" onchange=\"obtenerGrupos();\">
                      <option>Elige un curso</option>
                      <option value=\"Ingles\">Ingles</option>
                      <option value=\"Frances\">Frances</option>
                      </select>
            </div>
         <div class=\"col-lg-4\">
                <h3 class=\"card-title\">Horarios</h3>
                <br>
                  ";
        // line 121
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "grupos", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
            <script type=\"text/javascript\">
            \$('#academia_inscripcionbundle_estudiante_grupos').html(\"<option>Elige un curso</option>\");/*Sirve para que no se carguen todos los horarios ya sea de ingles o frances al cargar la pag*/

            </script>
          <hr>
              <div class=\"col-md-8\"></div>
              <div class=\"col-md-2\">
                <td>
                  <br><br><br><br>
                    <button type=\"submit\" class=\"btn btn-block btn-success fa fa-check\">&nbsp; Aceptar</button>
                </td>
              </div>
              <div class=\"col-md-2 \">
                <td>
                  <br><br><br><br>
                <a href=\"";
        // line 138
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_index");
        echo "\" class=\"btn btn-block btn-danger fa fa-close\">Cancelar</a>
                </td>
              </div>
              ";
        // line 141
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "


          </div>
         </div>
    </div>
  </div>
  ";
    }

    public function getTemplateName()
    {
        return ":estudiante:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 141,  200 => 138,  180 => 121,  152 => 96,  140 => 87,  126 => 76,  114 => 67,  100 => 56,  87 => 46,  77 => 39,  70 => 34,  68 => 33,  65 => 32,  41 => 11,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":estudiante:new.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/estudiante/new.html.twig");
    }
}
