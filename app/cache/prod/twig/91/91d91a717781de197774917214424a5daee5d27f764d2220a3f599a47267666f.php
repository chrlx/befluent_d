<?php

/* estudiante/listadoPorGrupo.html.twig */
class __TwigTemplate_de3a02cabd8b37c05d980eab083f6d9a9a7e76e2a5ed9637cd941cb9f8ef16e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "estudiante/listadoPorGrupo.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"content-wrapper card\"> ";
        // line 5
        echo "<div class=\"containers\">
    <div class=\"row\">
       <div class=\"col-12\">
           <div class=\"bg-color p-3 mb-2 text-white\" >
                     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
             <center><h3 class=\"titulo\">Alumnos inscritos en el grupo </h3></center>
            </div>

           <!-- /.card-header -->
           <div class=\"card-body table-responsive p-0\">
             <table class=\"table table-hover table-bordered\">
               <thead>
                   <tr>
                       <th>Nombre</th>
                       <th>Telefono</th>
                       <th>Email</th>
                       <th>Actions</th>
                   </tr>
               </thead>
               <tbody>
               ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["estudiante"]) {
            // line 28
            echo "                   <tr>
                       <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["estudiante"], "nombre", array()), "html", null, true);
            echo " <br></td>
                       <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["estudiante"], "telefono", array()), "html", null, true);
            echo " <br></td>
                       <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["estudiante"], "email", array()), "html", null, true);
            echo " <br></td>
                           <td>
                                    <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_show", array("id" => $this->getAttribute($context["estudiante"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>
                                    <a href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_edit", array("id" => $this->getAttribute($context["estudiante"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/edit.ico"), "html", null, true);
            echo "\" alt=\"Editar\" class=\"ico\"></a>
                           </td>
                   </tr>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['estudiante'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "               </tbody>
             </table>
             </div>
             <br>
             <hr>
             <div class=\"navigation\">
                ";
        // line 44
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : null));
        echo "
             </div>
           <!-- /.card-body -->
         </div>
         <!-- /.card -->
       </div>
     </div><!-- /.row -->
   </div><!-- /.container-fluid -->


  </div> ";
    }

    public function getTemplateName()
    {
        return "estudiante/listadoPorGrupo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 44,  98 => 38,  86 => 34,  80 => 33,  75 => 31,  71 => 30,  67 => 29,  64 => 28,  60 => 27,  40 => 10,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "estudiante/listadoPorGrupo.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\estudiante\\listadoPorGrupo.html.twig");
    }
}
