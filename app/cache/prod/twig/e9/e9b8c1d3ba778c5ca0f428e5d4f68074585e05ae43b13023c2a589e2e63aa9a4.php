<?php

/* :docente:new.html.twig */
class __TwigTemplate_9822d33b56f230d164176934e4f81eeac857f598b05ef975869dce30813e5c86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":docente:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"content-wrapper card\"> ";
        // line 5
        echo "<div class=\"containers\">
      <div class=\"p-3 mb-2 bg-color text-white\">
                 <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
      <center><h1 class=\"titulo\">Nuevo Docente</h1></center>
    </div>
    <div class=\"card-body\">
      ";
        // line 13
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("role" => "form")));
        echo "
               <h3 class=\"card-title\">Información Personal</h3>
               <br>
               <div class=\"row\">
                  <div class=\"col-sm-3 col-md-6 col-lg-12\">
                    <div class=\"input-group mb-3\">
                      <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
                      </div>
                      ";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Nombre Completo", "onpaste" => "return false", "required" => "true")));
        echo "
                     <!-- text input <input type=\"text\" onkeypress=\"return soloLetras(event)\" onpaste=\"return false\" class=\"form-control\" placeholder=\"Nombres\">-->
                    </div>
                   </div>
               </div>
               <div class=\"row\">
                 <div class=\"col-sm-3 col-md-4 col-lg-6\">
                   <div class=\"input-group mb-3\">
                     <div class=\"input-group-prepend\">
                       <span class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></span>
                     </div>
                       ";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edad", array()), 'widget', array("attr" => array("type" => "number", "class" => "form-control", "onkeypress" => "return isNumberKey(event)", "placeholder" => "Edad", "onpaste" => "return false", "min" => "18", "max" => "100", "onpaste" => "return false")));
        echo "
                   </div>
                   </div>
                   <div class=\"col-sm-3 col-md-4 col-lg-6\">
                     <div class=\"input-group mb-3\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-address-card-o\"></i></span>
                          </div>
                          ";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "dui", array()), 'widget', array("attr" => array("type" => "text", "data-inputmask" => "'mask' : ['99999999-9']", "data-mask" => "", "class" => "form-control", "placeholder" => "DUI", "onpaste" => "return false")));
        echo "
                          <!--<input type=\"text\" class=\"form-control\" data-inputmask=\"'mask': ['9999-999999-999-9', '99999999-9' ]\" data-mask placeholder=\"DUI, menor de edad NIT\">-->
                        </div>
                 </div>
               </div>
       <div class=\"row\">
          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
                <span class=\"input-group-text\"><i class=\"fa fa-envelope\"></i></span>
              </div>
              ";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("type" => "email", "class" => "form-control", "placeholder" => "Email")));
        echo "
              <!--<input type=\"email\" class=\"form-control\" placeholder=\"Email\">-->
            </div>
          </div>
          <div class=\"col-sm-3  col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
                 <div class=\"input-group-prepend\">
                   <span class=\"input-group-text\"><i class=\"fa fa-phone\"></i></span>
                 </div>
                 ";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "telefono", array()), 'widget', array("attr" => array("type" => "text", "data-inputmask" => "'mask': ['99999999', '9999-9999', '(+999) 9999-9999']", "data-mask" => "", "class" => "form-control", "placeholder" => "Telefono (agregar extencion antepone un +)", "onpaste" => "return false")));
        echo "
                 <!--<input type=\"text\" class=\"form-control\"  data-inputmask=\"'mask': [ '(+999) 9999-9999','9999-9999']\" data-mask placeholder=\"Numero de contacto\">-->
               </div>
          </div>
        </div>
  </div>
</div>
</div>
<div class=\"content-wrapper card\">
  <div class=\"containers\">
      <div class=\"card-body\">
        <h3 class=\"card-title\">Especialización</h3>
        <br>
        <div class=\"row\">
           <div class=\"col-sm-3 col-md-6 col-lg-12\">
             <div class=\"input-group mb-3\">
               <div class=\"input-group-prepend\">
                 <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
               </div>
               ";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "especializacion", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Especialización", "onpaste" => "return false")));
        echo "
              <!-- text input <input type=\"text\" onkeypress=\"return soloLetras(event)\" onpaste=\"return false\" class=\"form-control\" placeholder=\"Nombres\">-->
             </div>
            </div>
        </div>
        <hr>
        <br>
          <div class=\"row justify-content-between\">

              <div class=\"col-md-2 offset-md-8\">
                  <button type=\"submit\" class=\"btn btn-block btn-success fa fa-check\">&nbsp;Acceptar</button>
              </div>
              <div class=\"col-lg-2 \">
                  <a href=\"";
        // line 93
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_index");
        echo "\"><button type=\"button\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp;Regresar</button></a>
              </div>
              ";
        // line 95
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "

          </div>
         </div>
    </div>
  </div>
";
    }

    public function getTemplateName()
    {
        return ":docente:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 95,  147 => 93,  131 => 80,  109 => 61,  97 => 52,  83 => 41,  72 => 33,  58 => 22,  46 => 13,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":docente:new.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/docente/new.html.twig");
    }
}
