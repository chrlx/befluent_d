<?php

/* grupo/index.html.twig */
class __TwigTemplate_666477d7eba5fd27e6c6e93b63585b3b6beb2269630d95a0c4e0280460910b93 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "grupo/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/css/adminlte.min.css"), "html", null, true);
        echo "\">

<div class=\"content-wrapper card\"> ";
        // line 7
        echo "<div class=\"containers\">

<div class=\"row\">
<div class=\"col-12\">
  <div  class=\" p-3 mb-2 bg-color text-white\" >
    <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
            <h3 class=\" text-center titulo \">Listado de Grupos</h3>
             <div class=\"col-md-1  offset-md-11\">
               <a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_new");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/new_group.png"), "html", null, true);
        echo "\" title=\"nuevo grupo\" alt=\"new_user\" class=\"iconew\"></a>
             </div>
           </div>
           <!-- /.card-header -->
           <div class=\"card-body table-responsive p-0\">
           Total records: ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "getTotalItemCount", array()), "html", null, true);
        echo "
             <table class=\"table table-hover table-bordered\">
               <thead>
                   <tr>
                     <th>Tipo</th>
                     <th>Nivel</th>
                     <th>Hora Inicio</th>
                     <th>Hora Fin</th>
                     <th>Salon</th>
                     ";
        // line 32
        echo "                     <th>Docente Asignado</th>
                     <th>Actions</th>

                    ";
        // line 43
        echo "                   </tr>
               </thead>
               <tbody>
               ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
            // line 47
            echo "                          <tr>
                             <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "tipo", array()), "html", null, true);
            echo "</td>
                             <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "nivel", array()), "html", null, true);
            echo "</td>
                             <td>";
            // line 50
            if ($this->getAttribute($context["grupo"], "horario", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["grupo"], "horario", array()), " H:i:s"), "html", null, true);
            }
            echo "</td>
                             <td>";
            // line 51
            if ($this->getAttribute($context["grupo"], "horarioFin", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["grupo"], "horarioFin", array()), " H:i:s"), "html", null, true);
            }
            echo "</td>
                             <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "salon", array()), "html", null, true);
            echo "</td>
                             ";
            // line 54
            echo "                             <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "coach", array()), "html", null, true);
            echo "
                               <td>
                                        <a href=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_show", array("id" => $this->getAttribute($context["grupo"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>
                                        <a href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_edit", array("id" => $this->getAttribute($context["grupo"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/edit.ico"), "html", null, true);
            echo "\" alt=\"Editar\" class=\"ico\"></a>
                                        <a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_listadoGrupo", array("id" => $this->getAttribute($context["grupo"], "id", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/list-user.png"), "html", null, true);
            echo "\" title=\"Lista de Alumnos\" alt=\"Ver\" class=\"ico\"> </a>
                                        <a href=\"";
            // line 59
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("evaluacion_new");
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/exam.png"), "html", null, true);
            echo "\" alt=\"examen\" class=\"ico\"></a>
                               </td>
                         </tr>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "               </tbody>
             </table>
             <div class=\"navigation\">
                ";
        // line 66
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : null));
        echo "
             </div>
       </div>
     </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "grupo/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 66,  151 => 63,  139 => 59,  133 => 58,  127 => 57,  121 => 56,  115 => 54,  111 => 52,  105 => 51,  99 => 50,  95 => 49,  91 => 48,  88 => 47,  84 => 46,  79 => 43,  74 => 32,  62 => 22,  52 => 17,  45 => 13,  37 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "grupo/index.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\grupo\\index.html.twig");
    }
}
