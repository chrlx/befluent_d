<?php

/* :docente:edit.html.twig */
class __TwigTemplate_96e5d9d67901cfb533cd78a7ebc956fd724f9cabefff22102479c9db0561c9d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":docente:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"content-wrapper card background-style\"> ";
        // line 6
        echo "<div class=\"containers\">
      <div class=\" p-3 mb-2 bg-color text-white \">
                 <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
      <center><h3 class=\"titulo\"><strong>Edición de Docente</strong></h3></center>
    </div>
    <div class=\"card-body\">
      ";
        // line 14
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_start', array("attr" => array("role" => "form")));
        echo "
               <h3 class=\"card-title\"><strong>Información Personal</strong> </h3>
               <br>

               <div class=\"row\">
                  <div class=\"col-sm-12 col-md-12 col-lg-12\">
                    <div class=\"input-group mb-3\">
                      <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
                      </div>
                      ";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "nombre", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Nombre Completo", "onpaste" => "return false")));
        echo "
                      <!--, 'disabled':''-->
                    </div>
                   </div>
               </div>
               <div class=\"row\">
                 <div class=\"col-sm-6 col-md-6 col-lg-6\">
                   <div class=\"input-group mb-3\">
                     <div class=\"input-group-prepend\">
                       <span class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></span>
                     </div>
                       ";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "edad", array()), 'widget', array("attr" => array("type" => "number", "class" => "form-control", "onkeypress" => "return isNumberKey(event)", "placeholder" => "Edad", "onpaste" => "return false", "min" => "18", "max" => "100", "onpaste" => "return false")));
        echo "
                   </div>
                   </div>
                   <div class=\"col-sm-6 col-md-6 col-lg-6\">
                     <div class=\"input-group mb-3\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-address-card-o\"></i></span>
                          </div>
                          ";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "dui", array()), 'widget', array("attr" => array("type" => "text", "data-inputmask" => "'mask' : '99999999-9'", "data-mask" => "", "class" => "form-control", "placeholder" => "DUI", "onpaste" => "return false")));
        echo "
                        </div>
                 </div>
               </div>
                   <div class=\"row\">
                      <div class=\"col-sm-6 col-md-6 col-lg-6\">
                        <div class=\"input-group mb-3\">
                          <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-envelope\"></i></span>
                          </div>
                          ";
        // line 53
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "email", array()), 'widget', array("attr" => array("type" => "email", "class" => "form-control", "placeholder" => "Email")));
        echo "
                        </div>
                      </div>
                      <div class=\"col-sm-6  col-md-6 col-lg-6\">
                        <div class=\"input-group mb-3\">
                             <div class=\"input-group-prepend\">
                               <span class=\"input-group-text\"><i class=\"fa fa-phone\"></i></span>
                             </div>
                             ";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "telefono", array()), 'widget', array("attr" => array("type" => "text", "data-inputmask" => "'mask': ['99999999','9999-9999', '(+999) 9999-9999']", "data-mask" => "", "class" => "form-control", "placeholder" => "Telefono (agregar extencion antepone un +)", "onpaste" => "return false")));
        echo "
                           </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
<div class=\"content-wrapper card background-style  \">
  <div class=\"containers\">
      <div class=\"card-body\">
        <h3 class=\"card-title\"><strong>Especialización</strong></h3>
        <br>
        <div class=\"row\">
           <div class=\"col-sm-12 col-md-12 col-lg-12\">
             <div class=\"input-group mb-3\">
               <div class=\"input-group-prepend\">
                 <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
               </div>
               ";
        // line 79
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "especializacion", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Especialización", "onpaste" => "return false")));
        echo "
              <!-- text input <input type=\"text\" onkeypress=\"return soloLetras(event)\" onpaste=\"return false\" class=\"form-control\" placeholder=\"Nombres\">-->
             </div>
             <br>
             <hr>
            </div>
        </div>
          <div class=\"row justify-content-between \">
              <div class=\"col-md-2 offset-md-6\">
                  <button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button>
                  <br>
              </div>
              <div class=\"col-md-1 col-lg-2 \">
                  <a href=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_show", array("iddocente" => $this->getAttribute((isset($context["docente"]) ? $context["docente"] : null), "iddocente", array()))), "html", null, true);
        echo "\"><button type=\"button\" class=\"btn btn-block btn-info fa fa-eye\">&nbsp; Ver Cambios</button></a>
                <br>
              </div>
              <div class=\"col-md-1 col-lg-2 \">
                  <a href=\"";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_index");
        echo "\"><button type=\"button\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Cancelar</button></a>
                  <br>
              </div>
              ";
        // line 99
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_end');
        echo "
          </div>
         </div>
       </div>
  </div>

";
    }

    public function getTemplateName()
    {
        return ":docente:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 99,  152 => 96,  145 => 92,  129 => 79,  108 => 61,  97 => 53,  84 => 43,  73 => 35,  59 => 24,  46 => 14,  38 => 9,  33 => 6,  31 => 5,  28 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":docente:edit.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/docente/edit.html.twig");
    }
}
