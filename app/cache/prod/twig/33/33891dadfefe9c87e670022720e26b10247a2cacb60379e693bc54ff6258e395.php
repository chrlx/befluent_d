<?php

/* usuario/index.html.twig */
class __TwigTemplate_143ced4118e2dd6063f73e1bfacdb27429e8b67ad4f45271e85a7640991b8ba1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "usuario/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"content-wrapper card\"> ";
        // line 5
        echo "        <div class=\"containers\">
           <div class=\" p-3 mb-2 bg-color text-white\" >
                     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
             <center><h3 class=\"titulo\">Listado de Usuarios</h3></center>
             <div class=\"col-md-1  offset-md-11\">
               <a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_new");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/users-add-user-icon.png"), "html", null, true);
        echo "\" alt=\"new_user\" class=\"iconew\"></a>
             </div>
             </div>
           </div>

           <div class=\"card-body  table-responsive p-0\">

                <table class=\" table table-hover table-bordered\">
                    <thead>
                        <tr>
                            ";
        // line 23
        echo "                            <th>Username</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            ";
        // line 27
        echo "                            <th>Role</th>
                            ";
        // line 29
        echo "                            <th>Createdat</th>
                            <th>Updatedat</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usuarios"]) ? $context["usuarios"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["usuario"]) {
            // line 36
            echo "                        <tr>
                            ";
            // line 38
            echo "                            <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "username", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "nombre", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "email", array()), "html", null, true);
            echo "</td>
                            ";
            // line 42
            echo "                            <td>
                                    ";
            // line 43
            if (($this->getAttribute($context["usuario"], "role", array()) == "ROLE_ADMIN")) {
                // line 44
                echo "                                        <strong>Administrador</strong></td>
                                    ";
            } elseif (($this->getAttribute(            // line 45
$context["usuario"], "role", array()) == "ROLE_USER")) {
                // line 46
                echo "                                        <strong>Profesor</strong></td>
                                        
                                    ";
            } elseif (($this->getAttribute(            // line 48
$context["usuario"], "role", array()) == "ROLE_RECEP")) {
                // line 49
                echo "                                        <strong>Recepcionista</strong>
                                       
                                    ";
            }
            // line 52
            echo "                            </td>

                            ";
            // line 55
            echo "
                            <td>";
            // line 56
            if ($this->getAttribute($context["usuario"], "createdAt", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["usuario"], "createdAt", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                            <td>";
            // line 57
            if ($this->getAttribute($context["usuario"], "updatedAt", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["usuario"], "updatedAt", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                            <td>

                                <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_show", array("id" => $this->getAttribute($context["usuario"], "id", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>

                                <a href=\"";
            // line 62
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_edit", array("id" => $this->getAttribute($context["usuario"], "id", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/edit.ico"), "html", null, true);
            echo "\" alt=\"Editar\" class=\"ico\"></a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usuario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                    </tbody>
                </table>
                ";
        // line 75
        echo "
            </div>
     </div><!-- /.row -->
   </div><!-- /.container-fluid -->
";
    }

    public function getTemplateName()
    {
        return "usuario/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 75,  157 => 66,  145 => 62,  138 => 60,  130 => 57,  124 => 56,  121 => 55,  117 => 52,  112 => 49,  110 => 48,  106 => 46,  104 => 45,  101 => 44,  99 => 43,  96 => 42,  92 => 40,  88 => 39,  83 => 38,  80 => 36,  76 => 35,  68 => 29,  65 => 27,  60 => 23,  45 => 12,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "usuario/index.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\usuario\\index.html.twig");
    }
}
