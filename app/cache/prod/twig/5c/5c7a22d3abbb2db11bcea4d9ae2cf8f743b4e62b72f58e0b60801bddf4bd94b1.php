<?php

/* @Academiainscripcion/Default/index.html.twig */
class __TwigTemplate_25c16e6c2e9cdc5c639fabb0cbb07041657d92b792927326e2016c1b8fe13c28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
<head>
\t<title>Registrarse</title>
\t<meta charset=\"UTF-8\">
\t<meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
\t<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/css/main.css"), "html", null, true);
        echo "\">
</head>
<body class=\"cover\" style=\"background-image: url(";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/imagenes/sideBar-font.jpg"), "html", null, true);
        echo ");\">
\t<form action=\"home.html\" method=\"\" autocomplete=\"off\" class=\"full-box logInForm\">
\t\t<p class=\"text-center text-muted\"><i class=\"zmdi zmdi-account-circle zmdi-hc-5x\"></i></p>
\t\t<p class=\"text-center text-muted text-uppercase\">Inicia sesión con tu cuenta</p>
\t\t<div class=\"form-group label-floating\">
\t\t  <label class=\"control-label\" for=\"UserEmail\">E-mail</label>
\t\t  <input class=\"form-control\" id=\"UserEmail\" type=\"email\">
\t\t  <p class=\"help-block\">Escribe tú E-mail</p>
\t\t</div>
\t\t<div class=\"form-group label-floating\">
\t\t  <label class=\"control-label\" for=\"UserPass\">Contraseña</label>
\t\t  <input class=\"form-control\" id=\"UserPass\" type=\"text\">
\t\t  <p class=\"help-block\">Escribe tú contraseña</p>
\t\t</div>
\t\t<div class=\"form-group text-center\">
\t\t\t<input type=\"submit\" value=\"Iniciar sesión\" class=\"btn btn-raised btn-success\">
\t\t</div>
\t</form>
\t<!--====== Scripts -->
\t<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/js/jquery-3.1.1.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/js/material.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/js/ripples.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/js/sweetalert2.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/js/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("./vendor/js/main.js"), "html", null, true);
        echo "\"></script>
\t<script>
\t\t\$.material.init();
\t</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "@Academiainscripcion/Default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 34,  74 => 33,  70 => 32,  66 => 31,  62 => 30,  58 => 29,  54 => 28,  32 => 9,  27 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Academiainscripcion/Default/index.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\src\\Academia\\inscripcionBundle\\Resources\\views\\Default\\index.html.twig");
    }
}
