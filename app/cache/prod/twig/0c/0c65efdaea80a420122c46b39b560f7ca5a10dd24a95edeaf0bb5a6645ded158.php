<?php

/* evaluacion/show.html.twig */
class __TwigTemplate_e3090073602ee8a3d882e8160823069054121a350a40bef01e4393baeb82c404 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "evaluacion/show.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    

<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>

<script type=\"text/javascript\" >
    var n1=0;
    var n2=0;
    var n3=0;
    var aux=0;
    ids=[];
   //var total=0;

 function obtenerTotal(id,x,idGrupo){
   
          /*  for (var i = 0; i < x; i++) {     
           name= document.getElementsByName(id)[0].value;
           //total=parseInt(name)+total;
           id++;
       }*/
       var idConvertido=parseInt(idGrupo)
       if(aux==0){
       \$.ajax({
        data:{'id':idConvertido},
        url:\"";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("evaluacion_ajax");
        echo "\",
        type: 'POST',
        dataType:'json',
        success: function(data){
            var total=0;
            var nuevoDato;
            for (var i = 0; i < data.length; i++) {
                nuevoDato=document.getElementsByName(data[i].id)[0].value;
                total=total+parseInt(nuevoDato);
                ids[i]=data[i].id;
            }
            totalInput.value=total;
            aux++;
        }
       });
     }else{
      total=0;
      for (var o = 0; o < ids.length; o++) {
           nuevoDato=document.getElementsByName(ids[o])[0].value;
        total=total+parseInt(nuevoDato);
        
      }
      totalInput.value=total;
     }
    }

 function obtenerId(id){
    i++;
    n1=id;
    alert(n1);
 }
</script>

";
    }

    // line 62
    public function block_body($context, array $blocks = array())
    {
        // line 63
        echo "   <div class=\"content-wrapper card\"> ";
        // line 64
        echo "<div class=\"containers\">
           <div class=\" p-3 mb-2 bg-color text-white\" >
                     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
             <center><h3 class=\"titulo\">Evaluaciones del grupo </h3></center>

             </div>
             ";
        // line 72
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["editForm"]) ? $context["editForm"] : null), 'form_start', array("attr" => array("role" => "form")));
        echo "
           </div>
           <div class=\"card-body  table-responsive p-0\">
             <table class=\" table table-hover table-bordered\">
               <thead>
                   <tr>
                     <th>Nombre</th>
                     <th>Ponderacion %</th>
                     <th>Grupo</th>

                   </tr>
               </thead>
               <tbody>

                ";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["evaluaciones"]) ? $context["evaluaciones"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["evaluacions"]) {
            // line 87
            echo "                   <tr>
                       <td><input type=\"text\" name=\"";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "nombre", array()), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "nombre", array()), "html", null, true);
            echo "\"></td>
                       <td><input type=\"number\" name=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "ponderacion", array()), "html", null, true);
            echo "\" max=\"100\" min=\"0\" onload=\"obtenerId();\" onchange=\"obtenerTotal(";
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "id", array()), "html", null, true);
            echo ",";
            echo twig_escape_filter($this->env, (isset($context["tamaño"]) ? $context["tamaño"] : null), "html", null, true);
            echo ",";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["evaluacions"], "idGrupo", array()), "id", array()), "html", null, true);
            echo ");\"></td>
                       <td>";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluacions"], "idGrupo", array()), "html", null, true);
            echo " </td>
                       <td>
                                <a href=\"";
            // line 92
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("evaluacion_edit", array("id" => $this->getAttribute($context["evaluacions"], "id", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>
                       </td>
                   </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evaluacions'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "                <tr>
                    <td >Total</td>
                    <td id=\"total\"><input id=\"totalInput\" type=\"number\" max=100 min=100</td>
                </tr>

                </script>
               </tbody>
             </table>

             <hr>
             <div class=\"col-md-2 offset-md-8\">
                  <button type=\"submit\" class=\"btn btn-block btn-success fa fa-edit\">&nbsp;Editar</button>
                  <a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_index");
        echo " \"  class=\"btn btn-block btn-danger fa fa-check \">&nbsp; Regresar</a>
              </div>
                <hr>

           </div>


           ";
        // line 115
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["editForm"]) ? $context["editForm"] : null), 'form_end');
        echo "
     </div><!-- /.row -->
   </div><!-- /.container-fluid -->



";
    }

    public function getTemplateName()
    {
        return "evaluacion/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 115,  187 => 108,  173 => 96,  161 => 92,  156 => 90,  144 => 89,  137 => 88,  134 => 87,  130 => 86,  113 => 72,  105 => 67,  100 => 64,  98 => 63,  95 => 62,  57 => 27,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "evaluacion/show.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\evaluacion\\show.html.twig");
    }
}
