<?php

/* grupo/misgrupos.html.twig */
class __TwigTemplate_feaf884c61936de5df2409246c0b452e25d1fb9802236eb57cb5cea447f01311 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "grupo/misgrupos.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/css/adminlte.min.css"), "html", null, true);
        echo "\">

<div class=\"content-wrapper card\"> ";
        // line 7
        echo "<div class=\"containers\">

    <div class=\"row\">
       <div class=\"col-12\">
           <div  class=\" p-3 mb-2 bg-color text-white\" >
             <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
            <h3 class=\" text-center titulo \">Mis Grupos Asignados</h3>
             
           </div>
           <!-- /.card-header -->
           <div class=\"card-body table-responsive p-0\">
           Total records: ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "getTotalItemCount", array()), "html", null, true);
        echo "
             <table class=\"table table-hover table-bordered\">
               <thead>
                   <tr>
                     <th>Tipo</th>
                     <th>Nivel</th>
                     <th>Hora Inicio</th>
                     <th>Hora Fin</th>
                     <th>Salon</th>
                     <th>Actions</th>
                   </tr>
               </thead>
               <tbody>
               ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
            // line 34
            echo "                          <tr>
                             <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "tipo", array()), "html", null, true);
            echo "</td>
                             <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "nivel", array()), "html", null, true);
            echo "</td>
                             <td>";
            // line 37
            if ($this->getAttribute($context["grupo"], "horario", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["grupo"], "horario", array()), " H:i:s"), "html", null, true);
            }
            echo "</td>
                             <td>";
            // line 38
            if ($this->getAttribute($context["grupo"], "horarioFin", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["grupo"], "horarioFin", array()), " H:i:s"), "html", null, true);
            }
            echo "</td>
                             <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "salon", array()), "html", null, true);
            echo "</td>

                               <td>
                                    <a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_show", array("id" => $this->getAttribute($context["grupo"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" title=\"Ver Estudiantes\" alt=\"Ver\" class=\"ico\"></a> 
                                    <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_listadoGrupo", array("id" => $this->getAttribute($context["grupo"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/list-user.png"), "html", null, true);
            echo "\" title=\"Lista de Alumnos\" alt=\"Ver\" class=\"ico\"></a>
                                    <a href=\"";
            // line 44
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("evaluacion_new");
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/exam.png"), "html", null, true);
            echo "\" title=\"Crear Evaluaciones\" alt=\"Editar\" class=\"ico\"></a>

                               </td>
                         </tr>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "               </tbody>
             </table>
             <div class=\"navigation\">
                ";
        // line 52
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : null));
        echo "
             </div>
       </div>
     </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "grupo/misgrupos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 52,  129 => 49,  116 => 44,  110 => 43,  104 => 42,  98 => 39,  92 => 38,  86 => 37,  82 => 36,  78 => 35,  75 => 34,  71 => 33,  55 => 20,  45 => 13,  37 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "grupo/misgrupos.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\grupo\\misgrupos.html.twig");
    }
}
