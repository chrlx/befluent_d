<?php

/* estudiante/index.html.twig */
class __TwigTemplate_94971db1564bdf94a8c4bf933329c049c7e45dcde92bbdd87d969fdb79ce344b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "estudiante/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"content-wrapper card\"> ";
        // line 5
        echo "<div class=\"containers\">
    <div class=\"row\">
       <div class=\"col-12\">
           <div class=\"bg-color p-3 mb-2 text-white\" >
                     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
             <center><h3 class=\"titulo\">  Listado de Inscripciones </h3></center>
             <div class=\"col-md-1  offset-md-11\">
               <a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_new");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/users-add-user-icon.png"), "html", null, true);
        echo "\" alt=\"new_user\" class=\"iconew\"></a>
             </div>
            </div>

           <!-- /.card-header -->
           <div class=\"card-body table-responsive p-0\">
             <table class=\"table table-hover table-bordered\">
               <thead>
                   <tr>
                       <th>Nombre</th>
                       <th>Edad</th>
                       <th>Dui</th>
                       <th>Telefono</th>
                       <th>Email</th>
                       <th>Curso</th>
                       <th>Nivel</th>
                       <th>Salon</th>
                       <th>Horario</th>
                       <th>Actions</th>
                   </tr>
               </thead>
               <tbody>
               ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["estudiante"]) {
            // line 37
            echo "                   <tr>
                       <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["estudiante"], "nombre", array()), "html", null, true);
            echo " <br></td>
                       <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["estudiante"], "edad", array()), "html", null, true);
            echo " <br></td>
                       <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["estudiante"], "dui", array()), "html", null, true);
            echo " <br></td>
                       <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["estudiante"], "telefono", array()), "html", null, true);
            echo " <br></td>
                       <td>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["estudiante"], "email", array()), "html", null, true);
            echo " <br></td>
                       <td>
                           ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["estudiante"], "grupos", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
                // line 45
                echo "                           ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "tipo", array()), "html", null, true);
                echo "
                           <br>
                           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "                           </td>
                       <td>
                           ";
            // line 50
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["estudiante"], "grupos", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
                // line 51
                echo "                           ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "Nivel", array()), "html", null, true);
                echo "
                           <br>
                           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "                           </td>
                       <td>
                           ";
            // line 56
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["estudiante"], "grupos", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
                // line 57
                echo "                           ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["grupo"], "salon", array()), "html", null, true);
                echo "
                           <br>
                           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "                           </td>
                        <td>
                           ";
            // line 62
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["estudiante"], "grupos", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
                // line 63
                echo "                           ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["grupo"], "horario", array()), " H:i:s"), "html", null, true);
                echo "
                           <br>
                           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "                           </td>
                           <td>
                                    <a href=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_show", array("id" => $this->getAttribute($context["estudiante"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>
                                    <a href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estudiante_edit", array("id" => $this->getAttribute($context["estudiante"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/edit.ico"), "html", null, true);
            echo "\" alt=\"Editar\" class=\"ico\"></a>
                           </td>
                   </tr>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['estudiante'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "               </tbody>
             </table>
             </div>
             <br>
             <hr>
             <div class=\"navigation\">
                ";
        // line 79
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : null));
        echo "
             </div>
           <!-- /.card-body -->
         </div>
         <!-- /.card -->
       </div>
     </div><!-- /.row -->
   </div><!-- /.container-fluid -->


  </div> ";
    }

    public function getTemplateName()
    {
        return "estudiante/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 79,  192 => 73,  180 => 69,  174 => 68,  170 => 66,  160 => 63,  156 => 62,  152 => 60,  142 => 57,  138 => 56,  134 => 54,  124 => 51,  120 => 50,  116 => 48,  106 => 45,  102 => 44,  97 => 42,  93 => 41,  89 => 40,  85 => 39,  81 => 38,  78 => 37,  74 => 36,  47 => 14,  40 => 10,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "estudiante/index.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\estudiante\\index.html.twig");
    }
}
