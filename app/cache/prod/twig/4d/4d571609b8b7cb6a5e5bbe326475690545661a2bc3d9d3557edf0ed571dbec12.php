<?php

/* :docente:index.html.twig */
class __TwigTemplate_d752950239615e86ddcde287c452df6f2873b5dbe9df29f5ad3663ed4867764b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":docente:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"content-wrapper card\"> ";
        // line 5
        echo "<div class=\"containers\">
           <div class=\" p-3 mb-2 bg-color text-white\" >
                     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
             <center><h3 class=\"titulo\">Listado de Docente</h3></center>
             <div class=\"col-md-1  offset-md-11\">
               <a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_new");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/users-add-user-icon.png"), "html", null, true);
        echo "\" alt=\"new_user\" class=\"iconew\"></a>
             </div>
             </div>
           </div>
           <div class=\"card-body  table-responsive p-0\">
             <table class=\" table table-hover table-bordered\">
               <thead>
                   <tr>
                     <th>Nombre</th>
                     <th>Edad</th>
                     <th>DUI</th>
                     <th>Teléfono</th>
                     <th>Email</th>
                     <th>Especialización</th>
                     <th>Acciones</th>
                   </tr>
               </thead>
               <tbody>
               ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["docente"]) {
            // line 31
            echo "                   <tr>
                       <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["docente"], "nombre", array()), "html", null, true);
            echo " </td>
                       <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["docente"], "edad", array()), "html", null, true);
            echo " </td>
                       <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["docente"], "dui", array()), "html", null, true);
            echo " </td>
                       <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["docente"], "telefono", array()), "html", null, true);
            echo " </td>
                       <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["docente"], "email", array()), "html", null, true);
            echo " </td>
                       <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["docente"], "especializacion", array()), "html", null, true);
            echo " </td>
                       <td>
                                <a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_show", array("iddocente" => $this->getAttribute($context["docente"], "iddocente", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>
                                <a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("docente_edit", array("iddocente" => $this->getAttribute($context["docente"], "iddocente", array()))), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/edit.ico"), "html", null, true);
            echo "\" alt=\"Editar\" class=\"ico\"></a>
                       </td>
                   </tr>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['docente'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "               </tbody>
             </table>
             <hr>
             <div class=\"navigation\">
                  ";
        // line 48
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : null));
        echo "
             </div>
           </div>
     </div><!-- /.row -->
   </div><!-- /.container-fluid -->
   ";
    }

    public function getTemplateName()
    {
        return ":docente:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 48,  118 => 44,  106 => 40,  100 => 39,  95 => 37,  91 => 36,  87 => 35,  83 => 34,  79 => 33,  75 => 32,  72 => 31,  68 => 30,  45 => 12,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":docente:index.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/docente/index.html.twig");
    }
}
