<?php

/* :grupo:show.html.twig */
class __TwigTemplate_7d2f08345b862ec5e4fbf8e35a2dd01dd5dca25b395514290d8a7eebc4efe3eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":grupo:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"content-wrapper card\"> ";
        // line 6
        echo "<div class=\"containers\">
  <div class=\"row\">
     <div class=\"col-12\">
   <div class=\"bg-color p-3 mb-2 text-white\" >
     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
    <h1 class=\"text-center titulo\">Grupo</h1>
    </div>
    <ul class=\"list-group\">
      <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DEL GRUPO</b></li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ID:</b> &nbsp;&nbsp;";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "id", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TIPO:</b>        &nbsp;&nbsp;";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "tipo", array()), "html", null, true);
        echo "</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NIVEL:</b> &nbsp;&nbsp;";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "nivel", array()), "html", null, true);
        echo " </li>
    <li class=\"list-group-item\"><b>HORA INICIO:</b> &nbsp;&nbsp;";
        // line 20
        if ($this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "horario", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "horario", array()), "H:i:s"), "html", null, true);
        }
        echo "</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>HORA FIN:</b> &nbsp;&nbsp;";
        // line 21
        if ($this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "horarioFin", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "horarioFin", array()), "H:i:s"), "html", null, true);
        }
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SALON:</b> &nbsp;&nbsp;";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "salon", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>MODALIDAD:</b> &nbsp;&nbsp;";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "modalidad", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DEL DOCENTE </b></li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NOMBRE:</b> &nbsp;&nbsp; ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "coach", array()), "html", null, true);
        echo "</li>
</ul>

              ";
        // line 28
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_ADMIN")) {
            // line 29
            echo "                 <div class=\"row justify-content-between\">
                       <div class=\"col-md-2 offset-md-6\">
      <td>
        <a href=\"";
            // line 32
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_index");
            echo "\"><button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
    </div>
    <div class=\"col-md-2 \">
      <td>
      <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_edit", array("id" => $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "id", array()))), "html", null, true);
            echo " \"  class=\"btn btn-block btn-info fa fa-edit\" >&nbsp; Editar</a>
    </td>
    </div>
    <div class=\"col-md-2\">
      ";
            // line 41
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
            echo "
          <button type=\"submit\"  class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Eliminar</button>
      ";
            // line 43
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
            echo "
    </div>
    <br><br>
    </div>
              ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 47
(isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_USER")) {
            // line 48
            echo "      <div class=\"col-md-2 offset-md-9\">
      <td>
        <br><br><br><br>
        <a href=\"";
            // line 51
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_misgrupos");
            echo "\"><button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
      <br><br>
    </div>
    ";
        }
        // line 56
        echo "  </div>
  </div>
</div>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return ":grupo:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 56,  130 => 51,  125 => 48,  123 => 47,  116 => 43,  111 => 41,  104 => 37,  96 => 32,  91 => 29,  89 => 28,  83 => 25,  78 => 23,  74 => 22,  68 => 21,  62 => 20,  58 => 19,  54 => 18,  50 => 17,  41 => 11,  34 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":grupo:show.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/grupo/show.html.twig");
    }
}
