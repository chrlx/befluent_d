<?php

/* :usuario:show.html.twig */
class __TwigTemplate_07ef075c8d1a95b4d7a0db10af7a1d4f3a54feb5b72e972656cc954f7fca452f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", ":usuario:show.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "
    <div class=\"content-wrapper card\"> ";
        // line 7
        echo "        <div class=\"containers\">
            
            <div class=\" p-3 mb-2 bg-color text-white\">
         <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
                <h1 class=\"text-center\" >Usuario</h1>
            </div>

            <ul class=\"list-group\">
                <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACIÓN DEL USUARIO</b></li>
                <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>USERNAME:</b>        &nbsp;&nbsp;";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "username", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NOMBRE:</b>        &nbsp;&nbsp;";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CORREO:</b> &nbsp;&nbsp;";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "email", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ROL:</b> &nbsp;&nbsp;";
        // line 21
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_ADMIN")) {
            // line 22
            echo "                                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Administrador"), "html", null, true);
            echo "
                        ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 23
(isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_USER")) {
            // line 24
            echo "                                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Profesor"), "html", null, true);
            echo "
                        ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 25
(isset($context["app"]) ? $context["app"] : null), "user", array()), "role", array()) == "ROLE_RECEP")) {
            // line 26
            echo "                                 ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Recepcionista"), "html", null, true);
            echo "
                        ";
        }
        // line 28
        echo "                <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ESTADO:</b> &nbsp;&nbsp;";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "isActive", array())) {
            echo "Activo";
        } else {
            echo "Inactivo";
        }
        echo "</li>
                <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>FECHA CREADO:</b> &nbsp;&nbsp;";
        // line 29
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "createdAt", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "createdAt", array()), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</li>
                <li class=\"list-group-item\">&nbsp;&nbsp;<b>FECHA ACTUALIZADO:</b> &nbsp;&nbsp;";
        // line 30
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "updatedAt", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "updatedAt", array()), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</li>
            </ul>
            <div class=\"row justify-content-between\">
                <div class=\"col-md-2 offset-md-6\">
                    <td>
                        <br><br><br><br>
                        <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
        echo "\"><button type=\"submit\" class=\"btn btn-block btn-success fa fa-check\">&nbsp; Aceptar</button></a>
                    </td>
                </div>
                <div class=\"col-md-2 \">
                    <td>
                        <br><br><br><br>
                        <a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_edit", array("id" => $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "id", array()))), "html", null, true);
        echo " \"  class=\"btn btn-block btn-info fa fa-edit\">&nbsp; Editar</a>
                    </td>
                </div>

                <div class=\"col-lg-2\">
                    <br><br><br><br>
                    ";
        // line 48
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
        echo "
                        <button type=\"submit\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Eliminar</button>
                    ";
        // line 50
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
        echo "
                </div>
            </div>
            <br>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return ":usuario:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 50,  128 => 48,  119 => 42,  110 => 36,  99 => 30,  93 => 29,  84 => 28,  78 => 26,  76 => 25,  71 => 24,  69 => 23,  64 => 22,  62 => 21,  58 => 20,  54 => 19,  50 => 18,  40 => 11,  34 => 7,  31 => 5,  28 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":usuario:show.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app/Resources\\views/usuario/show.html.twig");
    }
}
