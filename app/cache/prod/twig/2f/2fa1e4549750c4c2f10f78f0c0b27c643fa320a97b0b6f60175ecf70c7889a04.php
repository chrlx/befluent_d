<?php

/* grupo/new.html.twig */
class __TwigTemplate_f862ccf66426bb61ed3fcafe9cc134a7510f0ee782e43acea424f9487812de37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "grupo/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "  <div class=\"content-wrapper card\"> ";
        // line 5
        echo "    <div class=\"containers\">
      <!-- /.card-header -->
      <div class=\" p-3 mb-2  bg-color \">
         <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
        <div class=\"card-body \">
          <h1 class=\" text-center titulo text-white\">Creación de Grupo</h1>
        </div>
      </div>

      ";
        // line 16
        if ((((isset($context["docenteOcupado"]) ? $context["docenteOcupado"] : null) == true) && ((isset($context["horariosEstanBien"]) ? $context["horariosEstanBien"] : null) == true))) {
            // line 17
            echo "        <p class=\"bg-danger text-white\" >El docente ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "coach", array()), "html", null, true);
            echo " ya tiene un grupo de ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "horario", array()), " H:i:s"), "html", null, true);
            echo " a ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["grupo"]) ? $context["grupo"] : null), "horarioFin", array()), " H:i:s"), "html", null, true);
            echo "</p>
      ";
        }
        // line 19
        echo "
      ";
        // line 20
        if (((isset($context["horariosEstanBien"]) ? $context["horariosEstanBien"] : null) == false)) {
            // line 21
            echo "        <p class=\"bg-danger text-white\" >La hora de inicio del grupo no debe ser igual o mayor que la hora fin de este</p>
      ";
        }
        // line 23
        echo "
      <div class=\"card-body\">

        ";
        // line 26
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("role" => "form")));
        echo "
        <h3 class=\"card-title\">Datos De Grupo</h3>
        <br>
        <!-- text input -->


        <!--************************* cursos: ingles/frances *********************************-->

        <div class=\"row\">
          <div class=\"col-sm-3 col-md-6 col-lg-6\">
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
              <span class=\"input-group-text\"><i class=\"fa fa-id-card\"></i></span>
              </div>
              ";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tipo", array()), 'widget', array("attr" => array("type" => "text", "onkeypress" => "return soloLetras(event)", "class" => "form-control", "placeholder" => "Tipo de curso", "onpaste" => "return false")));
        echo "
              <!-- text input <input type=\"text\" onkeypress=\"return soloLetras(event)\" onpaste=\"return false\" class=\"form-control\" placeholder=\"Nombres\">-->
            </div>
          </div>


          <!-- ************************ hora inicio **************************************** -->

          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <div class=\"bootstrap-timepicker\">
              <!-- <div class=\"form-group row\">
                <label for=\"example-time-input\" class=\"col-2 col-form-label\">Hora Inicio</label>
                <span class=\"input-group-text\"><i class=\"fa fa-clock-o\"></i></span>
                <div class=\"col-8\">
                  <input class=\"form-control\" type=\"time\" value=\"13:45:00\" id=\"example-time-input\">
                </div>
              </div>-->

              <div class=\"input-group-append\">
                <label for=\"example-time-input\" class=\"col-3 col-form-label\">Hora Inicio</label>
                <span class=\"input-group-text\"><i class=\"fa fa-clock-o\"></i></span>
                ";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "horario", array()), 'widget', array("attr" => array("type" => "time", "value" => "13:45:00", "id" => "example-time-input", "class" => "form-control")));
        echo "
                <!--<input type=\"text\" class=\"form-control\" data-inputmask=\"'mask': ['9999-999999-999-9', '99999999-9' ]\" data-mask placeholder=\"DUI, menor de edad NIT\">-->
              </div>
              <!-- /.input group -->
            </div>
          </div>
        </div>

        <!-- *************************************Nivel : basico/intermedio/avanzado************************* -->

        <div class=\"row\">
          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
                <span class=\"input-group-text\"><i class=\"fa fa-spinner\"></i></span>
              </div>
              ";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nivel", array()), 'widget', array("attr" => array("type" => "text", "class" => "form-control", "placeholder" => "Nivel", "onpaste" => "return false")));
        echo "
              <!-- <input type=\"text\" class=\"form-control\" data-inputmask=\"'alias': 'dd/mm/yyyy'\" data-mask placeholder=\"Fecha de nacimientos\">-->
            </div>
          </div>
          <!-- ******************************Hora fin************************ -->

          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <div class=\"bootstrap-timepicker\">
              <div class=\"input-group-append\">
                <label for=\"example-time-input\" class=\"col-3 col-form-label\">Hora Fin</label>
                <span class=\"input-group-text\"><i class=\"fa fa-clock-o\"></i></span>
                ";
        // line 88
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "horarioFin", array()), 'widget', array("attr" => array("type" => "time", "value" => "13:45:00", "id" => "example-time-input", "class" => "form-control")));
        echo "
                <!--<input type=\"text\" class=\"form-control\" data-inputmask=\"'mask': ['9999-999999-999-9', '99999999-9' ]\" data-mask placeholder=\"DUI, menor de edad NIT\">-->
              </div>

             <!-- <div  class=\"md-form\">
  <input type=\"text\" id=\"manual-operations-input\" class=\"form-control\" placeholder=\"Now\">
  <label for=\"form1\" class=\"\">Check the minutes</label>
</div>

              <div class=\"md-form\">
  <input type=\"text\" class=\"form-control timepicker\" placeholder=\"Now\">
  <label for=\"form1\" class=\"\">Check the minutes</label>
</div>-->
              <!-- /.input group -->
            </div>
          </div>
        </div>    

        <!-- ******************************* Salon***************************************** -->
        <div class=\"row\">
          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
                <span class=\"input-group-text\"><i class=\"fa fa-group\"></i></span>
              </div>
              ";
        // line 113
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "salon", array()), 'widget', array("attr" => array("type" => "email", "class" => "form-control", "placeholder" => "Salon")));
        echo "
              <!--<input type=\"email\" class=\"form-control\" placeholder=\"Email\">-->
            </div>
          </div>

          <!-- ********************************* recupera el usuario ***************** -->
                    
          <div class=\"col-sm-3  col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
              <div class=\"input-group-prepend\">
                <span class=\"input-group-text\"><i class=\"fa fa-user\"></i></span>
              </div>
              ";
        // line 125
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coach", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
          </div>
        </div>                                                 
        
        <!-- ***************************** Modalidad ********************************* -->
        
        <div class=\"row\">
          <div class=\"col-sm-3 col-md-4 col-lg-6\">
            <div class=\"input-group mb-3\">
                <div class=\"input-group-prepend\">
                  <span class=\"input-group-text\"><i class=\"fa fa-group\"></i></span>
                </div>
                ";
        // line 138
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "modalidad", array()), 'widget', array("attr" => array("type" => "email", "class" => "form-control", "placeholder" => "Salon")));
        echo "
                <!--<input type=\"email\" class=\"form-control\" placeholder=\"Email\">-->
              </div>
            </div>                    
          </div>
        </div>  
      </div>
    </div>
  </div>
<br> <br>
  <div class=\"card-body\">
    <div class=\"row justify-content-between\">
      <div class=\"col-md-2 offset-md-7\">
        <td>
          <button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button>
        </td>
      </div>
      <div class=\"col-md-2 \">
        <td>
          <a href=\"";
        // line 157
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("grupo_index");
        echo "\"><button type=\"button\" class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Cancelar</button></a>
        </td>
      </div>
      ";
        // line 160
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "

    </div>
  </div>
";
    }

    public function getTemplateName()
    {
        return "grupo/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 160,  230 => 157,  208 => 138,  192 => 125,  177 => 113,  149 => 88,  135 => 77,  116 => 61,  92 => 40,  75 => 26,  70 => 23,  66 => 21,  64 => 20,  61 => 19,  51 => 17,  49 => 16,  39 => 9,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "grupo/new.html.twig", "C:\\xampp\\htdocs\\app\\Befluent\\app\\Resources\\views\\grupo\\new.html.twig");
    }
}
