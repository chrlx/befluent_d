<?php

namespace Proxies\__CG__\Academia\inscripcionBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Docente extends \Academia\inscripcionBundle\Entity\Docente implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'iddocente', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'nombre', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'edad', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'dui', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'telefono', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'email', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'especializacion'];
        }

        return ['__isInitialized__', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'iddocente', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'nombre', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'edad', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'dui', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'telefono', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'email', '' . "\0" . 'Academia\\inscripcionBundle\\Entity\\Docente' . "\0" . 'especializacion'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Docente $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getIddocente()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getIddocente();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIddocente', []);

        return parent::getIddocente();
    }

    /**
     * {@inheritDoc}
     */
    public function setNombre($nombre)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNombre', [$nombre]);

        return parent::setNombre($nombre);
    }

    /**
     * {@inheritDoc}
     */
    public function getNombre()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNombre', []);

        return parent::getNombre();
    }

    /**
     * {@inheritDoc}
     */
    public function setEdad($edad)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEdad', [$edad]);

        return parent::setEdad($edad);
    }

    /**
     * {@inheritDoc}
     */
    public function getEdad()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEdad', []);

        return parent::getEdad();
    }

    /**
     * {@inheritDoc}
     */
    public function setDui($dui)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDui', [$dui]);

        return parent::setDui($dui);
    }

    /**
     * {@inheritDoc}
     */
    public function getDui()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDui', []);

        return parent::getDui();
    }

    /**
     * {@inheritDoc}
     */
    public function setTelefono($telefono)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTelefono', [$telefono]);

        return parent::setTelefono($telefono);
    }

    /**
     * {@inheritDoc}
     */
    public function getTelefono()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTelefono', []);

        return parent::getTelefono();
    }

    /**
     * {@inheritDoc}
     */
    public function setEmail($email)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmail', [$email]);

        return parent::setEmail($email);
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmail', []);

        return parent::getEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function setEspecializacion($especializacion)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEspecializacion', [$especializacion]);

        return parent::setEspecializacion($especializacion);
    }

    /**
     * {@inheritDoc}
     */
    public function getEspecializacion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEspecializacion', []);

        return parent::getEspecializacion();
    }

    /**
     * {@inheritDoc}
     */
    public function addGrupo(\Academia\inscripcionBundle\Entity\Grupo $grupo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addGrupo', [$grupo]);

        return parent::addGrupo($grupo);
    }

    /**
     * {@inheritDoc}
     */
    public function removeGrupo(\Academia\inscripcionBundle\Entity\Grupo $grupo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeGrupo', [$grupo]);

        return parent::removeGrupo($grupo);
    }

    /**
     * {@inheritDoc}
     */
    public function getGrupo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getGrupo', []);

        return parent::getGrupo();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', []);

        return parent::__toString();
    }

}
