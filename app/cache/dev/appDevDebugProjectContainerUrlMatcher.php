<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === rtrim($pathinfo, '/')) {
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                        goto not__profiler_home;
                    } else {
                        return $this->redirect($rawPathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ('/_profiler/purge' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/estudiante')) {
            // estudiante_index
            if ('/estudiante' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_estudiante_index;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'estudiante_index');
                }

                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_estudiante_index;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EstudianteController::indexAction',  '_route' => 'estudiante_index',);
            }
            not_estudiante_index:

            // estudiante_show
            if (preg_match('#^/estudiante/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_estudiante_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'estudiante_show')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EstudianteController::showAction',));
            }
            not_estudiante_show:

            // estudiante_new
            if ('/estudiante/new' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_estudiante_new;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EstudianteController::newAction',  '_route' => 'estudiante_new',);
            }
            not_estudiante_new:

            // estudiante_edit
            if (preg_match('#^/estudiante/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_estudiante_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'estudiante_edit')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EstudianteController::editAction',));
            }
            not_estudiante_edit:

            // estudiante_delete
            if (preg_match('#^/estudiante/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_estudiante_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'estudiante_delete')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EstudianteController::deleteAction',));
            }
            not_estudiante_delete:

            // estudiante_ajax
            if ('/estudiante/ajax' === $pathinfo) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_estudiante_ajax;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EstudianteController::obtenerAction',  '_route' => 'estudiante_ajax',);
            }
            not_estudiante_ajax:

            // estudiante_consulta
            if ('/estudiante/consulta' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_estudiante_consulta;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EstudianteController::consultaAction',  '_route' => 'estudiante_consulta',);
            }
            not_estudiante_consulta:

        }

        if (0 === strpos($pathinfo, '/grupo')) {
            // grupo_index
            if ('/grupo' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_grupo_index;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'grupo_index');
                }

                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_grupo_index;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::indexAction',  '_route' => 'grupo_index',);
            }
            not_grupo_index:

            // grupo_show
            if (preg_match('#^/grupo/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_grupo_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grupo_show')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::showAction',));
            }
            not_grupo_show:

            // grupo_new
            if ('/grupo/new' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_grupo_new;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::newAction',  '_route' => 'grupo_new',);
            }
            not_grupo_new:

            // grupo_edit
            if (preg_match('#^/grupo/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_grupo_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grupo_edit')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::editAction',));
            }
            not_grupo_edit:

            // grupo_delete
            if (preg_match('#^/grupo/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_grupo_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grupo_delete')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::deleteAction',));
            }
            not_grupo_delete:

            if (0 === strpos($pathinfo, '/grupo/ingles')) {
                // grupo_consultaIngles
                if ('/grupo/ingles' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grupo_consultaIngles;
                    }

                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::consultaInglesAction',  '_route' => 'grupo_consultaIngles',);
                }
                not_grupo_consultaIngles:

                // grupo_consultaInglesBasico
                if ('/grupo/ingles/basico' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grupo_consultaInglesBasico;
                    }

                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::consultaInglesBasicoAction',  '_route' => 'grupo_consultaInglesBasico',);
                }
                not_grupo_consultaInglesBasico:

                // grupo_consultaInglesIntermedio
                if ('/grupo/ingles/intermedio' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grupo_consultaInglesIntermedio;
                    }

                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::consultaInglesIntermedioAction',  '_route' => 'grupo_consultaInglesIntermedio',);
                }
                not_grupo_consultaInglesIntermedio:

                // grupo_consultaInglesAvanzado
                if ('/grupo/ingles/avanzado' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grupo_consultaInglesAvanzado;
                    }

                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::consultaInglesAvanzadoAction',  '_route' => 'grupo_consultaInglesAvanzado',);
                }
                not_grupo_consultaInglesAvanzado:

            }

            if (0 === strpos($pathinfo, '/grupo/frances')) {
                // grupo_consultaFrances
                if ('/grupo/frances' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grupo_consultaFrances;
                    }

                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::consultaFrancesAction',  '_route' => 'grupo_consultaFrances',);
                }
                not_grupo_consultaFrances:

                // grupo_consultaFrancesBasico
                if ('/grupo/frances/basico' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grupo_consultaFrancesBasico;
                    }

                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::consultaFrancesBasicoAction',  '_route' => 'grupo_consultaFrancesBasico',);
                }
                not_grupo_consultaFrancesBasico:

                // grupo_consultaFrancesIntermedio
                if ('/grupo/frances/intermedio' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grupo_consultaFrancesIntermedio;
                    }

                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::consultaFrancesIntermedioAction',  '_route' => 'grupo_consultaFrancesIntermedio',);
                }
                not_grupo_consultaFrancesIntermedio:

                // grupo_consultaFrancesAvanzado
                if ('/grupo/frances/avanzado' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grupo_consultaFrancesAvanzado;
                    }

                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::consultaFrancesAvanzadoAction',  '_route' => 'grupo_consultaFrancesAvanzado',);
                }
                not_grupo_consultaFrancesAvanzado:

            }

            // grupo_misgrupos
            if ('/grupo/misgrupos' === $pathinfo) {
                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::misgruposAction',  '_route' => 'grupo_misgrupos',);
            }

            // grupo_listadoGrupo
            if (preg_match('#^/grupo/(?P<id>[^/]++)/listadoGrupo$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_grupo_listadoGrupo;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grupo_listadoGrupo')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\GrupoController::listadoGrupoAction',));
            }
            not_grupo_listadoGrupo:

        }

        if (0 === strpos($pathinfo, '/docente')) {
            // docente_index
            if ('/docente' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_docente_index;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'docente_index');
                }

                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_docente_index;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\DocenteController::indexAction',  '_route' => 'docente_index',);
            }
            not_docente_index:

            // docente_show
            if (preg_match('#^/docente/(?P<iddocente>[^/]++)/show$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_docente_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'docente_show')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\DocenteController::showAction',));
            }
            not_docente_show:

            // docente_new
            if ('/docente/new' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_docente_new;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\DocenteController::newAction',  '_route' => 'docente_new',);
            }
            not_docente_new:

            // docente_edit
            if (preg_match('#^/docente/(?P<iddocente>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_docente_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'docente_edit')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\DocenteController::editAction',));
            }
            not_docente_edit:

            // docente_delete
            if (preg_match('#^/docente/(?P<iddocente>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_docente_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'docente_delete')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\DocenteController::deleteAction',));
            }
            not_docente_delete:

        }

        if (0 === strpos($pathinfo, '/evaluacion')) {
            // evaluacion_index
            if ('/evaluacion/index' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_evaluacion_index;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EvaluacionController::indexAction',  '_route' => 'evaluacion_index',);
            }
            not_evaluacion_index:

            // evaluacion_show
            if (preg_match('#^/evaluacion/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_evaluacion_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluacion_show')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EvaluacionController::showAction',));
            }
            not_evaluacion_show:

            // evaluacion_new
            if ('/evaluacion/new' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_evaluacion_new;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EvaluacionController::newAction',  '_route' => 'evaluacion_new',);
            }
            not_evaluacion_new:

            // evaluacion_edit
            if (preg_match('#^/evaluacion/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_evaluacion_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluacion_edit')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EvaluacionController::editAction',));
            }
            not_evaluacion_edit:

            // evaluacion_delete
            if (preg_match('#^/evaluacion/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_evaluacion_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluacion_delete')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EvaluacionController::deleteAction',));
            }
            not_evaluacion_delete:

            // evaluacion_ajax
            if ('/evaluacion/ajax' === $pathinfo) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_evaluacion_ajax;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EvaluacionController::obtenerAction',  '_route' => 'evaluacion_ajax',);
            }
            not_evaluacion_ajax:

            // evaluacion_crear
            if (preg_match('#^/evaluacion/(?P<id>[^/]++)/crear$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_evaluacion_crear;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluacion_crear')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EvaluacionController::crearAction',));
            }
            not_evaluacion_crear:

            // evaluacion_agregar
            if ('/evaluacion/ajax' === $pathinfo) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_evaluacion_agregar;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\EvaluacionController::agregarEvaluacionAction',  '_route' => 'evaluacion_agregar',);
            }
            not_evaluacion_agregar:

        }

        if (0 === strpos($pathinfo, '/usuario')) {
            // usuario_index
            if ('/usuario' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_usuario_index;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'usuario_index');
                }

                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_usuario_index;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::indexAction',  '_route' => 'usuario_index',);
            }
            not_usuario_index:

            // usuario_show
            if (preg_match('#^/usuario/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_usuario_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_show')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::showAction',));
            }
            not_usuario_show:

            // usuario_new
            if ('/usuario/new' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_usuario_new;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::newAction',  '_route' => 'usuario_new',);
            }
            not_usuario_new:

            // usuario_edit
            if (preg_match('#^/usuario/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_usuario_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_edit')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::editAction',));
            }
            not_usuario_edit:

            // usuario_delete
            if (preg_match('#^/usuario/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_usuario_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_delete')), array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::deleteAction',));
            }
            not_usuario_delete:

            // usuario_consultaUsuarioAdmin
            if ('/usuario/admin' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_usuario_consultaUsuarioAdmin;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::consultaUsuarioAdminAction',  '_route' => 'usuario_consultaUsuarioAdmin',);
            }
            not_usuario_consultaUsuarioAdmin:

            // usuario_consultaUsuarioDocente
            if ('/usuario/docente' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_usuario_consultaUsuarioDocente;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::consultaUsuarioDocenteAction',  '_route' => 'usuario_consultaUsuarioDocente',);
            }
            not_usuario_consultaUsuarioDocente:

            // usuario_consultaUsuarioRecepcionista
            if ('/usuario/recepcionista' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_usuario_consultaUsuarioRecepcionista;
                }

                return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::consultaUsuarioRecepcionistaAction',  '_route' => 'usuario_consultaUsuarioRecepcionista',);
            }
            not_usuario_consultaUsuarioRecepcionista:

        }

        // academiainscripcion_homepage
        if ('' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not_academiainscripcion_homepage;
            } else {
                return $this->redirect($rawPathinfo.'/', 'academiainscripcion_homepage');
            }

            return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\UsuarioController::homeAction',  '_route' => 'academiainscripcion_homepage',);
        }
        not_academiainscripcion_homepage:

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // academiainscripcion_login
                if ('/login' === $pathinfo) {
                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\SecurityController::loginAction',  '_route' => 'academiainscripcion_login',);
                }

                // academiainscripcion_login_check
                if ('/login_check' === $pathinfo) {
                    return array (  '_controller' => 'Academia\\inscripcionBundle\\Controller\\SecurityController::loginCheckAction',  '_route' => 'academiainscripcion_login_check',);
                }

            }

            // academiainscripcion_logout
            if ('/logout' === $pathinfo) {
                return array('_route' => 'academiainscripcion_logout');
            }

        }

        // homepage
        if ('' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not_homepage;
            } else {
                return $this->redirect($rawPathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }
        not_homepage:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
