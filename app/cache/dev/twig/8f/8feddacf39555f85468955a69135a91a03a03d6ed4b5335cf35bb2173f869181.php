<?php

/* grupo/show.html.twig */
class __TwigTemplate_6869748688498d8c31cfa5b60b954a6c2e7ccc3a8d2c05fb50b8477dd03440fe extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "grupo/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "grupo/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"content-wrapper card\"> ";
        // line 6
        echo "<div class=\"containers\">
  <div class=\"row\">
     <div class=\"col-12\">
   <div class=\"bg-color p-3 mb-2 text-white\" >
     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
    <h1 class=\"text-center titulo\">Grupo</h1>
    </div>
    <ul class=\"list-group\">
      <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DEL GRUPO</b></li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ID:</b> &nbsp;&nbsp;";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 17, $this->source); })()), "id", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TIPO:</b>        &nbsp;&nbsp;";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 18, $this->source); })()), "tipo", array()), "html", null, true);
        echo "</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NIVEL:</b> &nbsp;&nbsp;";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 19, $this->source); })()), "nivel", array()), "html", null, true);
        echo " </li>
    <li class=\"list-group-item\"><b>HORA INICIO:</b> &nbsp;&nbsp;";
        // line 20
        if (twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 20, $this->source); })()), "horario", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 20, $this->source); })()), "horario", array()), "H:i:s"), "html", null, true);
        }
        echo "</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>HORA FIN:</b> &nbsp;&nbsp;";
        // line 21
        if (twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 21, $this->source); })()), "horarioFin", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 21, $this->source); })()), "horarioFin", array()), "H:i:s"), "html", null, true);
        }
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SALON:</b> &nbsp;&nbsp;";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 22, $this->source); })()), "salon", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>MODALIDAD:</b> &nbsp;&nbsp;";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 23, $this->source); })()), "modalidad", array()), "html", null, true);
        echo "</li>
  <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DEL DOCENTE </b></li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NOMBRE:</b> &nbsp;&nbsp; ";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 25, $this->source); })()), "coach", array()), "html", null, true);
        echo "</li>
</ul>

              ";
        // line 28
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 28, $this->source); })()), "user", array()), "role", array()) == "ROLE_ADMIN")) {
            // line 29
            echo "                 <div class=\"row justify-content-between\">
                       <div class=\"col-md-2 offset-md-6\">
      <td>
        <a href=\"";
            // line 32
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_index");
            echo "\"><button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
    </div>
    <div class=\"col-md-2 \">
      <td>
      <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_edit", array("id" => twig_get_attribute($this->env, $this->source, (isset($context["grupo"]) || array_key_exists("grupo", $context) ? $context["grupo"] : (function () { throw new Twig_Error_Runtime('Variable "grupo" does not exist.', 37, $this->source); })()), "id", array()))), "html", null, true);
            echo " \"  class=\"btn btn-block btn-info fa fa-edit\" >&nbsp; Editar</a>
    </td>
    </div>
    <div class=\"col-md-2\">
      ";
            // line 41
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 41, $this->source); })()), 'form_start');
            echo "
          <button type=\"submit\"  class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Eliminar</button>
      ";
            // line 43
            echo             $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 43, $this->source); })()), 'form_end');
            echo "
    </div>
    <br><br>
    </div>
              ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 47
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 47, $this->source); })()), "user", array()), "role", array()) == "ROLE_USER")) {
            // line 48
            echo "      <div class=\"col-md-2 offset-md-9\">
      <td>
        <br><br><br><br>
        <a href=\"";
            // line 51
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_misgrupos");
            echo "\"><button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
      <br><br>
    </div>
    ";
        }
        // line 56
        echo "  </div>
  </div>
</div>
</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "grupo/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 56,  143 => 51,  138 => 48,  136 => 47,  129 => 43,  124 => 41,  117 => 37,  109 => 32,  104 => 29,  102 => 28,  96 => 25,  91 => 23,  87 => 22,  81 => 21,  75 => 20,  71 => 19,  67 => 18,  63 => 17,  54 => 11,  47 => 6,  44 => 4,  38 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}

<div class=\"content-wrapper card\"> {# para ponerlo a la par de la plantilla #}
<div class=\"containers\">
  <div class=\"row\">
     <div class=\"col-12\">
   <div class=\"bg-color p-3 mb-2 text-white\" >
     <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"{{asset('vendor/imagenes/menu.png')}}\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
    <h1 class=\"text-center titulo\">Grupo</h1>
    </div>
    <ul class=\"list-group\">
      <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DEL GRUPO</b></li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ID:</b> &nbsp;&nbsp;{{ grupo.id }}</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TIPO:</b>        &nbsp;&nbsp;{{ grupo.tipo }}</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NIVEL:</b> &nbsp;&nbsp;{{ grupo.nivel }} </li>
    <li class=\"list-group-item\"><b>HORA INICIO:</b> &nbsp;&nbsp;{% if grupo.horario %}{{ grupo.horario|date('H:i:s') }}{% endif %}</li>
    <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>HORA FIN:</b> &nbsp;&nbsp;{% if grupo.horarioFin %}{{ grupo.horarioFin|date('H:i:s') }}{% endif %}</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SALON:</b> &nbsp;&nbsp;{{ grupo.salon }}</li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>MODALIDAD:</b> &nbsp;&nbsp;{{ grupo.modalidad }}</li>
  <li class=\"list-group-item p-3 mb-2 bg-info text-white\"> <b> INFORMACION DEL DOCENTE </b></li>
  <li class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NOMBRE:</b> &nbsp;&nbsp; {{ grupo.coach}}</li>
</ul>

              {% if app.user.role == 'ROLE_ADMIN' %}
                 <div class=\"row justify-content-between\">
                       <div class=\"col-md-2 offset-md-6\">
      <td>
        <a href=\"{{ path('grupo_index') }}\"><button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
    </div>
    <div class=\"col-md-2 \">
      <td>
      <a href=\"{{ path('grupo_edit', { 'id': grupo.id }) }} \"  class=\"btn btn-block btn-info fa fa-edit\" >&nbsp; Editar</a>
    </td>
    </div>
    <div class=\"col-md-2\">
      {{ form_start(delete_form) }}
          <button type=\"submit\"  class=\"btn btn-block btn-danger fa fa-close\">&nbsp; Eliminar</button>
      {{ form_end(delete_form) }}
    </div>
    <br><br>
    </div>
              {% elseif app.user.role == 'ROLE_USER' %}
      <div class=\"col-md-2 offset-md-9\">
      <td>
        <br><br><br><br>
        <a href=\"{{ path('grupo_misgrupos') }}\"><button type=\"submit\" class=\"btn btn-block btn-primary fa fa-check\">&nbsp; Aceptar</button></a>
      </td>
      <br><br>
    </div>
    {% endif %}
  </div>
  </div>
</div>
</div>
</div>
{% endblock %}", "grupo/show.html.twig", "C:\\Users\\Christian\\Documents\\dsi2\\proy\\2sprint\\Nueva carpeta\\Befluent\\app\\Resources\\views\\grupo\\show.html.twig");
    }
}
