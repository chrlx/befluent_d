<?php

/* usuario/home.html.twig */
class __TwigTemplate_f03558ee83be68cfaab50b626f8a7e2cce96c58054f20337a47be38644161fbb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("principal.html.twig", "usuario/home.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "usuario/home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
\t<div class=\"container\">
\t\t<nav class=\"navbar navbar-light navbar-expand-md navigation-clean-button\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<h1 class=\"text-center\">Befluent - Bienvenido</h1>
\t\t\t\t\t<h3 class=\"text-center\">
\t\t\t\t\t\t";
        // line 10
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 10, $this->source); })()), "user", array()), "role", array()) == "ROLE_ADMIN")) {
            // line 11
            echo "\t\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Administrador"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 12
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 12, $this->source); })()), "user", array()), "role", array()) == "ROLE_USER")) {
            // line 13
            echo "\t\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Profesor"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 14
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 14, $this->source); })()), "user", array()), "role", array()) == "ROLE_RECEP")) {
            // line 15
            echo "    \t";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Recepcionista"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        }
        // line 17
        echo "\t\t\t\t\t\t\t :\t";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 17, $this->source); })()), "user", array()), "username", array()), "html", null, true);
        echo "
\t\t\t\t\t\t</h3>
\t\t\t\t</div>\t

\t\t\t<div class=\"col-xs-11 col-md-11\"></div>
\t\t\t\t<div class=\"col-xs-1 col-md-1\">
\t\t\t\t\t<a href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("academiainscripcion_logout");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/logout.png"), "html", null, true);
        echo "\" alt=\"logout\" title=\"logout\" class=\"iconew\"></a>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br> 
             </div>
\t\t</nav>

\t</div>
\t<div class=\"container\">
\t\t<div  class=\"full-box text-center\" style=\"padding: 30px 10px;\">
\t\t\t";
        // line 33
        echo "     \t\t";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 34
            echo "\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Asistencia</div>
\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t<i class=\"fa fa-line-chart fa-8x\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"";
            // line 49
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_index");
            echo "\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Grupos</div>
\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t<i class=\"fa fa-list-ul fa-10x\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"";
            // line 64
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("usuario_index");
            echo "\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Usuarios</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-slideshare fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t";
            // line 73
            echo "\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"";
            // line 80
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("estudiante_index");
            echo "\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Studiantes</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-weixin fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t";
        }
        // line 94
        echo "\t\t\t
\t\t\t";
        // line 96
        echo "\t\t\t ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 97
            echo "\t\t\t <div class=\"col-md-4\"></div>
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t<a href=\"";
            // line 99
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_misgrupos");
            echo "\">
\t\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Grupos Asignados</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-list-ul fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        }
        // line 114
        echo "
\t\t\t";
        // line 116
        echo "     \t\t";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_RECEP")) {
            // line 117
            echo "
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"";
            // line 119
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("estudiante_index");
            echo "\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Inscripciones</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-weixin fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<small>Nueva Inscripcion</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t";
        }
        // line 132
        echo "\t\t</div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "usuario/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 132,  213 => 119,  209 => 117,  206 => 116,  203 => 114,  185 => 99,  181 => 97,  178 => 96,  175 => 94,  158 => 80,  149 => 73,  138 => 64,  120 => 49,  103 => 34,  100 => 33,  86 => 23,  76 => 17,  70 => 15,  68 => 14,  63 => 13,  61 => 12,  56 => 11,  54 => 10,  44 => 4,  38 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'principal.html.twig' %}

{% block body %}
\t{{ parent() }}
\t<div class=\"container\">
\t\t<nav class=\"navbar navbar-light navbar-expand-md navigation-clean-button\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<h1 class=\"text-center\">Befluent - Bienvenido</h1>
\t\t\t\t\t<h3 class=\"text-center\">
\t\t\t\t\t\t{% if app.user.role == 'ROLE_ADMIN' %}
\t\t\t\t\t\t\t\t{{ 'Administrador'|trans }}
\t\t\t\t\t\t{% elseif app.user.role == 'ROLE_USER' %}
\t\t\t\t\t\t\t\t{{ 'Profesor'|trans }}
\t\t\t\t\t\t{% elseif app.user.role == 'ROLE_RECEP' %}
    \t{{ 'Recepcionista'|trans }}
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t :\t{{ app.user.username }}
\t\t\t\t\t\t</h3>
\t\t\t\t</div>\t

\t\t\t<div class=\"col-xs-11 col-md-11\"></div>
\t\t\t\t<div class=\"col-xs-1 col-md-1\">
\t\t\t\t\t<a href=\"{{ path('academiainscripcion_logout') }}\"><img src=\"{{asset('vendor/imagenes/logout.png')}}\" alt=\"logout\" title=\"logout\" class=\"iconew\"></a>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br> 
             </div>
\t\t</nav>

\t</div>
\t<div class=\"container\">
\t\t<div  class=\"full-box text-center\" style=\"padding: 30px 10px;\">
\t\t\t{# solamente si el usuario logueado es un administrador mostrara todo lo que esta dentro del if#}
     \t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Asistencia</div>
\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t<i class=\"fa fa-line-chart fa-8x\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"{{path('grupo_index')}}\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Grupos</div>
\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t<i class=\"fa fa-list-ul fa-10x\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"{{path('usuario_index')}}\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Usuarios</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-slideshare fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t{#<!--<p class=\"full-box\">{{pagination.getTotalItemCount}}</p>-->#}
\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"{{path('estudiante_index')}}\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Studiantes</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-weixin fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t{% endif %}
\t\t\t
\t\t\t{# solamente si el usuario logueado es un Docente mostrara todo lo que esta dentro del if#}
\t\t\t {% if is_granted('ROLE_USER') %}
\t\t\t <div class=\"col-md-4\"></div>
\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t<a href=\"{{path('grupo_misgrupos')}}\">
\t\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Grupos Asignados</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-list-ul fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<p class=\"full-box\">70</p>
\t\t\t\t\t\t\t\t<small>Registrados</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t{% endif %}

\t\t\t{# solamente si el usuario logueado es un Docente mostrara todo lo que esta dentro del if#}
     \t\t{% if is_granted('ROLE_RECEP') %}

\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<a href=\"{{path('estudiante_index')}}\">
\t\t\t\t\t\t<article class=\"full-box tile\">
\t\t\t\t\t\t\t<div class=\"full-box tile-title text-center text-titles text-uppercase\">Inscripciones</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-icon text-center\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-weixin fa-10x\"></i>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"full-box tile-number text-titles\">
\t\t\t\t\t\t\t\t<small>Nueva Inscripcion</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</article>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t{% endif %}
\t\t</div>
</div>

{% endblock %}
", "usuario/home.html.twig", "C:\\Users\\Christian\\Documents\\dsi2\\proy\\2sprint\\Nueva carpeta\\Befluent\\app\\Resources\\views\\usuario\\home.html.twig");
    }
}
