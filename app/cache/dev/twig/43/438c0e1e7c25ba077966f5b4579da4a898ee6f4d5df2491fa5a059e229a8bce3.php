<?php

/* base.html.twig */
class __TwigTemplate_2c4328cd25c33a68d066c59a48f008a08975b35e2b76445f11de98104f17e15a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/css/adminlte.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/css/font-awesome.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/css/mystyle.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/iCheck/flat/blue.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/morris/morris.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/jvectormap/jquery-jvectormap-1.2.2.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/datepicker/datepicker3.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/daterangepicker/daterangepicker-bs3.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/datatables/dataTables.bootstrap4.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("https://use.fontawesome.com/releases/v5.3.1/css/all.css"), "html", null, true);
        echo "\" integrity=\"sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.7/css/mdb.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/timepicker/bootstrap-timepicker.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/timepicker/bootstrap-timepicker.css"), "html", null, true);
        echo "\">
        <style media=\"screen\">


          li {list-style-type: none;}
        </style>
        ";
        // line 29
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 30
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("sigaabef.ico"), "html", null, true);
        echo "\" />
    </head>
    <body onload=\"menuDesplegar()\">
     <aside class=\"main-sidebar sidebar-dark-primary elevation-4\">
       <a href=\"#\" class=\"brand-link\">
       <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("sigaabef.png"), "html", null, true);
        echo "\" alt=\"befluent\" class=\"brand-image img-circle elevation-3\" style=\"opacity: .8\">
       <span class=\"brand-text font-weight-light\">BeFluent</span>
      </a>


   <!-- Sidebar -->
   <div class=\"sidebar\">
     <!-- Sidebar user panel (optional) -->
     <div class=\"user-panel mt-2 pb-2 mb-2 d-flex\">
       <div class=\"image\">
         <img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("sigaabef.png"), "html", null, true);
        echo "\" class=\"img-circle elevation-2\" alt=\"User Image\">
       </div>

      <a href=\"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("academiainscripcion_homepage");
        echo "\" title=\"Home\"> <div class=\"info text-white\">
       ";
        // line 49
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 49, $this->source); })()), "user", array()), "role", array()) == "ROLE_ADMIN")) {
            // line 50
            echo "                ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Administrador"), "html", null, true);
            echo "
            ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 51
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 51, $this->source); })()), "user", array()), "role", array()) == "ROLE_USER")) {
            // line 52
            echo "                ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Profesor"), "html", null, true);
            echo "
            ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 53
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 53, $this->source); })()), "user", array()), "role", array()) == "ROLE_RECEP")) {
            // line 54
            echo "                ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Recepcionista"), "html", null, true);
            echo "
            ";
        }
        // line 56
        echo "       </div>
       </a>
     </div>

     <!-- Sidebar Menu -->
     <nav class=\"mt-2\">
     ";
        // line 63
        echo "     ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 64
            echo "       <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">
        <!--Navegacion de ususarios-->
         <li class=\"nav-item\">
           <a href=\"";
            // line 67
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("usuario_index");
            echo "\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-user\"></i>
             <p>Usuarios
             <i class=\"fa fa-angle-left right\"></i></p>
           </a>
           <ul  class=\"nav nav-treeview\">
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-users-cog\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"";
            // line 75
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("usuario_consultaUsuarioAdmin");
            echo "\">Administrador</a>
             </li>
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-chalkboard-teacher\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"";
            // line 79
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("usuario_consultaUsuarioDocente");
            echo "\">Docentes</a>
             </li>
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-user-alt\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"";
            // line 83
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("usuario_consultaUsuarioRecepcionista");
            echo "\">Recepcionistas</a>
             </li>
           </ul>
         </li>
         <!--Navegacion de Grupos-->
         <li class=\"nav-item has-treeview toc-entry toc-h2\">
           <a href=\"#\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-group\"></i>
             <p>Grupo
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
            <ul class=\"nav nav-treeview\">
              <li class=\"nav-item\">
                <i class=\"nav-icon fab fa-etsy\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 98
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_consultaIngles");
            echo "\">Inglés</a>
              </li>
            <ul>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-empty \" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 103
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_consultaInglesBasico");
            echo "\">Básico</a>
              </li>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-half\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 107
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_consultaInglesIntermedio");
            echo "\">Intermedio</a>
              </li>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-full\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 111
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_consultaInglesAvanzado");
            echo "\">Avanzado</a>
              </li>
            </ul>
              <li class=\"nav-item\">
                <i class=\"fas fa-map-pin nav-icon\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"";
            // line 116
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_consultaFrances");
            echo "\">Francés</a>
              </li>
              <ul>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-empty\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"";
            // line 121
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_consultaFrancesBasico");
            echo "\" >Básico</a>
                </li>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-half\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"";
            // line 125
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_consultaFrancesIntermedio");
            echo "\" >Intermedio</a>
                </li>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-full\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"";
            // line 129
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_consultaFrancesAvanzado");
            echo "\" >Avanzado</a>
                </li>
              </ul>
            </ul>
         </li>

         <li class=\"nav-item has-treeview\">
           <a href=\"";
            // line 136
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("estudiante_index");
            echo "\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-edit\"></i>
             <p>
               Inscripcion
               <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>
         ";
            // line 155
            echo "        </ul>
      ";
        }
        // line 157
        echo "
      ";
        // line 159
        echo "      ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 160
            echo "        <li class=\"nav-item  has-treeview toc-entry toc-h2\">
           <a href=\"";
            // line 161
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_misgrupos");
            echo "\" class=\"nav-link\">
            <p><i class=\"nav-icon fa fa-group\"></i>
             Mis Grupos
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>
      ";
        }
        // line 169
        echo "
      ";
        // line 171
        echo "      ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_RECEP")) {
            // line 172
            echo "        <li class=\"nav-item  has-treeview\">
           <a href=\"";
            // line 173
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("estudiante_index");
            echo "\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-group\"></i>
             <p>Inscripcion
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>

      ";
        }
        // line 182
        echo "

     </nav>
   </div>
 </aside>


        ";
        // line 189
        $this->displayBlock('body', $context, $blocks);
        // line 190
        echo "
        <script>
  \$.widget.bridge('uibutton', \$.ui.button);
</script>


        <script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/datatables/dataTables.bootstrap4.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/jquery/jquery.slim.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/jQueryUI/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/bootstrap/js/bootstrap.bundle.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/select2/select2.full.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/input-mask/jquery.inputmask.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/input-mask/jquery.inputmask.date.extensions.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 204
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/input-mask/jquery.inputmask.extensions.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 205
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/daterangepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/colorpicker/bootstrap-colorpicker.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/timepicker/bootstrap-timepicker.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 208
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/timepicker/bootstrap-timepicker.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/slimScroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/iCheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/plugins/fastclick/fastclick.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/jquery/myScript.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/js/adminlte.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/js/pages/dashboard.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/js/demo.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.7/js/mdb.min.js"), "html", null, true);
        echo "\"></script>

        ";
        // line 220
        $this->displayBlock('javascripts', $context, $blocks);
        // line 221
        echo "    </body>
</html>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "BeFluent";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 29
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 189
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 220
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  504 => 220,  493 => 189,  482 => 29,  470 => 5,  461 => 221,  459 => 220,  454 => 218,  450 => 217,  446 => 216,  442 => 215,  438 => 214,  434 => 213,  430 => 212,  426 => 211,  422 => 210,  418 => 209,  414 => 208,  410 => 207,  406 => 206,  402 => 205,  398 => 204,  394 => 203,  390 => 202,  386 => 201,  382 => 200,  378 => 199,  374 => 198,  370 => 197,  366 => 196,  358 => 190,  356 => 189,  347 => 182,  335 => 173,  332 => 172,  329 => 171,  326 => 169,  315 => 161,  312 => 160,  309 => 159,  306 => 157,  302 => 155,  291 => 136,  281 => 129,  274 => 125,  267 => 121,  259 => 116,  251 => 111,  244 => 107,  237 => 103,  229 => 98,  211 => 83,  204 => 79,  197 => 75,  186 => 67,  181 => 64,  178 => 63,  170 => 56,  164 => 54,  162 => 53,  157 => 52,  155 => 51,  150 => 50,  148 => 49,  144 => 48,  138 => 45,  125 => 35,  116 => 30,  114 => 29,  105 => 23,  101 => 22,  97 => 21,  93 => 20,  89 => 19,  85 => 18,  81 => 17,  77 => 16,  73 => 15,  69 => 14,  65 => 13,  61 => 12,  57 => 11,  53 => 10,  49 => 9,  45 => 8,  41 => 7,  36 => 5,  30 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}BeFluent{% endblock %}</title>

        <link rel=\"stylesheet\" href=\"{{asset('vendor/css/adminlte.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/css/font-awesome.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/css/mystyle.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/iCheck/flat/blue.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/morris/morris.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/datepicker/datepicker3.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/daterangepicker/daterangepicker-bs3.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/datatables/dataTables.bootstrap4.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/bootstrap/css/bootstrap.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('https://use.fontawesome.com/releases/v5.3.1/css/all.css')}}\" integrity=\"sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"{{asset('https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.7/css/mdb.min.css')}}\" />
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/timepicker/bootstrap-timepicker.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('vendor/plugins/timepicker/bootstrap-timepicker.css')}}\">
        <style media=\"screen\">


          li {list-style-type: none;}
        </style>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('sigaabef.ico') }}\" />
    </head>
    <body onload=\"menuDesplegar()\">
     <aside class=\"main-sidebar sidebar-dark-primary elevation-4\">
       <a href=\"#\" class=\"brand-link\">
       <img src=\"{{asset('sigaabef.png')}}\" alt=\"befluent\" class=\"brand-image img-circle elevation-3\" style=\"opacity: .8\">
       <span class=\"brand-text font-weight-light\">BeFluent</span>
      </a>


   <!-- Sidebar -->
   <div class=\"sidebar\">
     <!-- Sidebar user panel (optional) -->
     <div class=\"user-panel mt-2 pb-2 mb-2 d-flex\">
       <div class=\"image\">
         <img src=\"{{asset('sigaabef.png')}}\" class=\"img-circle elevation-2\" alt=\"User Image\">
       </div>

      <a href=\"{{path('academiainscripcion_homepage')}}\" title=\"Home\"> <div class=\"info text-white\">
       {% if app.user.role == 'ROLE_ADMIN' %}
                {{ 'Administrador'|trans }}
            {% elseif app.user.role == 'ROLE_USER' %}
                {{ 'Profesor'|trans }}
            {% elseif app.user.role == 'ROLE_RECEP' %}
                {{ 'Recepcionista'|trans }}
            {% endif %}
       </div>
       </a>
     </div>

     <!-- Sidebar Menu -->
     <nav class=\"mt-2\">
     {# solamente si el usuario logueado es un administrador mostrara todo lo que esta dentro del if#}
     {% if is_granted('ROLE_ADMIN') %}
       <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">
        <!--Navegacion de ususarios-->
         <li class=\"nav-item\">
           <a href=\"{{path('usuario_index')}}\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-user\"></i>
             <p>Usuarios
             <i class=\"fa fa-angle-left right\"></i></p>
           </a>
           <ul  class=\"nav nav-treeview\">
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-users-cog\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"{{path('usuario_consultaUsuarioAdmin')}}\">Administrador</a>
             </li>
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-chalkboard-teacher\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"{{path('usuario_consultaUsuarioDocente')}}\">Docentes</a>
             </li>
             <li class=\"nav-item\">
               <i class=\"nav-icon fas fa-user-alt\" style=\"color:white; opacity: 0.8;\"></i>
               <a href=\"{{path('usuario_consultaUsuarioRecepcionista')}}\">Recepcionistas</a>
             </li>
           </ul>
         </li>
         <!--Navegacion de Grupos-->
         <li class=\"nav-item has-treeview toc-entry toc-h2\">
           <a href=\"#\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-group\"></i>
             <p>Grupo
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
            <ul class=\"nav nav-treeview\">
              <li class=\"nav-item\">
                <i class=\"nav-icon fab fa-etsy\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"{{path('grupo_consultaIngles')}}\">Inglés</a>
              </li>
            <ul>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-empty \" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"{{path('grupo_consultaInglesBasico')}}\">Básico</a>
              </li>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-half\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"{{path('grupo_consultaInglesIntermedio')}}\">Intermedio</a>
              </li>
              <li class=\"nav-item\">
                <i class=\"nav-icon fas fa-battery-full\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"{{path('grupo_consultaInglesAvanzado')}}\">Avanzado</a>
              </li>
            </ul>
              <li class=\"nav-item\">
                <i class=\"fas fa-map-pin nav-icon\" style=\"color:white; opacity: 0.8;\"></i>
                <a href=\"{{path('grupo_consultaFrances')}}\">Francés</a>
              </li>
              <ul>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-empty\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"{{path('grupo_consultaFrancesBasico')}}\" >Básico</a>
                </li>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-half\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"{{path('grupo_consultaFrancesIntermedio')}}\" >Intermedio</a>
                </li>
                <li class=\"nav-item\">
                  <i class=\"nav-icon fas fa-battery-full\" style=\"color:white; opacity: 0.8;\"></i>
                  <a href=\"{{path('grupo_consultaFrancesAvanzado')}}\" >Avanzado</a>
                </li>
              </ul>
            </ul>
         </li>

         <li class=\"nav-item has-treeview\">
           <a href=\"{{path('estudiante_index')}}\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-edit\"></i>
             <p>
               Inscripcion
               <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>
         {#
         <li class=\"nav-item has-treeview\">
           <a href=\"{{path('docente_index')}}\" class=\"nav-link\">
             <i class=\"fas fa-chalkboard-teacher nav-icon\"></i>
             <p>
               Docentes
               <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>
         #}
        </ul>
      {% endif %}

      {# solamente los usuarios de tipo profesor veran lo que esta en este if #}
      {% if is_granted('ROLE_USER') %}
        <li class=\"nav-item  has-treeview toc-entry toc-h2\">
           <a href=\"{{path('grupo_misgrupos')}}\" class=\"nav-link\">
            <p><i class=\"nav-icon fa fa-group\"></i>
             Mis Grupos
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>
      {% endif %}

      {# solamente los usuarios de tipo recepcionista veran lo que esta en este if #}
      {% if is_granted('ROLE_RECEP') %}
        <li class=\"nav-item  has-treeview\">
           <a href=\"{{path('estudiante_index')}}\" class=\"nav-link\">
             <i class=\"nav-icon fa fa-group\"></i>
             <p>Inscripcion
             <i class=\"fa fa-angle-left right\"></i>
             </p>
           </a>
         </li>

      {% endif %}


     </nav>
   </div>
 </aside>


        {% block body %}{% endblock %}

        <script>
  \$.widget.bridge('uibutton', \$.ui.button);
</script>


        <script src=\"{{asset('vendor/plugins/datatables/dataTables.bootstrap4.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/jquery/jquery.slim.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/jquery/jquery.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/jQueryUI/jquery-ui.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/bootstrap/js/bootstrap.bundle.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/select2/select2.full.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/input-mask/jquery.inputmask.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/input-mask/jquery.inputmask.date.extensions.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/input-mask/jquery.inputmask.extensions.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/daterangepicker/daterangepicker.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/colorpicker/bootstrap-colorpicker.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/timepicker/bootstrap-timepicker.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/timepicker/bootstrap-timepicker.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/slimScroll/jquery.slimscroll.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/datatables/jquery.dataTables.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/iCheck/icheck.min.js')}}\"></script>
        <script src=\"{{asset('vendor/plugins/fastclick/fastclick.js')}}\"></script>
        <script src=\"{{asset('vendor/jquery/myScript.js')}}\"></script>
        <script src=\"{{asset('vendor/js/adminlte.min.js')}}\"></script>
        <script src=\"{{asset('vendor/js/pages/dashboard.js')}}\"></script>
        <script src=\"{{asset('vendor/js/demo.js')}}\"></script>
        <script src=\"{{asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js')}}\"></script>
        <script src=\"{{asset('https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.7/js/mdb.min.js')}}\"></script>

        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "C:\\Users\\Christian\\Documents\\dsi2\\proy\\2sprint\\Nueva carpeta\\Befluent\\app\\Resources\\views\\base.html.twig");
    }
}
