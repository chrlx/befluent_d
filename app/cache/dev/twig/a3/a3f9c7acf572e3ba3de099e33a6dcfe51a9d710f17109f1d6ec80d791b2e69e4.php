<?php

/* grupo/index.html.twig */
class __TwigTemplate_057d3f86d9546673a640c7c0e23da2412a0ac57508da3b3edc5e6e4a6dcd05c6 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "grupo/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "grupo/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/css/adminlte.min.css"), "html", null, true);
        echo "\">

<div class=\"content-wrapper card\"> ";
        // line 7
        echo "<div class=\"containers\">

<div class=\"row\">
<div class=\"col-12\">
  <div  class=\" p-3 mb-2 bg-color text-white\" >
    <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/menu.png"), "html", null, true);
        echo "\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
            <h3 class=\" text-center titulo \">Listado de Grupos</h3>
             <div class=\"col-md-1  offset-md-11\">
               <a href=\"";
        // line 17
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_new");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/new_group.png"), "html", null, true);
        echo "\" title=\"nuevo grupo\" alt=\"new_user\" class=\"iconew\"></a>
             </div>
           </div>
           <!-- /.card-header -->
           <div class=\"card-body table-responsive p-0\">
           Total records: ";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new Twig_Error_Runtime('Variable "pagination" does not exist.', 22, $this->source); })()), "getTotalItemCount", array()), "html", null, true);
        echo "
             <table class=\"table table-hover table-bordered\">
               <thead>
                   <tr>
                     <th>Tipo</th>
                     <th>Nivel</th>
                     <th>Hora Inicio</th>
                     <th>Hora Fin</th>
                     <th>Salon</th>
                     ";
        // line 32
        echo "                     <th>Docente Asignado</th>
                     <th>Actions</th>

                    ";
        // line 43
        echo "                   </tr>
               </thead>
               <tbody>
               ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new Twig_Error_Runtime('Variable "pagination" does not exist.', 46, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["grupo"]) {
            // line 47
            echo "                          <tr>
                             <td>";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["grupo"], "tipo", array()), "html", null, true);
            echo "</td>
                             <td>";
            // line 49
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["grupo"], "nivel", array()), "html", null, true);
            echo "</td>
                             <td>";
            // line 50
            if (twig_get_attribute($this->env, $this->source, $context["grupo"], "horario", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["grupo"], "horario", array()), " H:i:s"), "html", null, true);
            }
            echo "</td>
                             <td>";
            // line 51
            if (twig_get_attribute($this->env, $this->source, $context["grupo"], "horarioFin", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["grupo"], "horarioFin", array()), " H:i:s"), "html", null, true);
            }
            echo "</td>
                             <td>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["grupo"], "salon", array()), "html", null, true);
            echo "</td>
                             ";
            // line 54
            echo "                             <td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["grupo"], "coach", array()), "html", null, true);
            echo "
                               <td>
                                        <a href=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_show", array("id" => twig_get_attribute($this->env, $this->source, $context["grupo"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/ver.ico"), "html", null, true);
            echo "\" alt=\"Ver\" class=\"ico\"></a>
                                        <a href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_edit", array("id" => twig_get_attribute($this->env, $this->source, $context["grupo"], "id", array()))), "html", null, true);
            echo "\" > <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/edit.ico"), "html", null, true);
            echo "\" alt=\"Editar\" class=\"ico\"></a>
                                        <a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("grupo_listadoGrupo", array("id" => twig_get_attribute($this->env, $this->source, $context["grupo"], "id", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/list-user.png"), "html", null, true);
            echo "\" title=\"Lista de Alumnos\" alt=\"Ver\" class=\"ico\"> </a>
                                        ";
            // line 60
            echo "                                        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("evaluacion_crear", array("id" => twig_get_attribute($this->env, $this->source, $context["grupo"], "id", array()))), "html", null, true);
            echo "\" > <img src=src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/exam.png"), "html", null, true);
            echo "\"  alt=\"examen\" class=\"ico\"></a>
                               </td>
                         </tr>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grupo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "               </tbody>
             </table>
             <div class=\"navigation\">
                ";
        // line 67
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new Twig_Error_Runtime('Variable "pagination" does not exist.', 67, $this->source); })()));
        echo "
             </div>
       </div>
     </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "grupo/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 67,  165 => 64,  152 => 60,  146 => 58,  140 => 57,  134 => 56,  128 => 54,  124 => 52,  118 => 51,  112 => 50,  108 => 49,  104 => 48,  101 => 47,  97 => 46,  92 => 43,  87 => 32,  75 => 22,  65 => 17,  58 => 13,  50 => 7,  44 => 4,  38 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<link rel=\"stylesheet\" href=\"{{ asset('vendor/css/adminlte.min.css')}}\">

<div class=\"content-wrapper card\"> {# para ponerlo a la par de la plantilla #}
<div class=\"containers\">

<div class=\"row\">
<div class=\"col-12\">
  <div  class=\" p-3 mb-2 bg-color text-white\" >
    <li class=\"nav-item\" >
      <a class=\"nav-link\" data-widget=\"pushmenu\" href=\"#\"><img src=\"{{asset('vendor/imagenes/menu.png')}}\" title=\"menu\" alt=\"new_user\" class=\"ico\"></a>
    </li>
            <h3 class=\" text-center titulo \">Listado de Grupos</h3>
             <div class=\"col-md-1  offset-md-11\">
               <a href=\"{{ path('grupo_new') }}\"><img src=\"{{asset('vendor/imagenes/new_group.png')}}\" title=\"nuevo grupo\" alt=\"new_user\" class=\"iconew\"></a>
             </div>
           </div>
           <!-- /.card-header -->
           <div class=\"card-body table-responsive p-0\">
           Total records: {{pagination.getTotalItemCount}}
             <table class=\"table table-hover table-bordered\">
               <thead>
                   <tr>
                     <th>Tipo</th>
                     <th>Nivel</th>
                     <th>Hora Inicio</th>
                     <th>Hora Fin</th>
                     <th>Salon</th>
                     {#<th>Docente Asignado</th>#}
                     <th>Docente Asignado</th>
                     <th>Actions</th>

                    {#
                     <th>{{ knp_pagination_sortable(pagination, 'Tipo', 'g.tipo') }}</th>
                     <th>{{ knp_pagination_sortable(pagination, 'Nivel', 'g.nivel') }}</th>
                     <th>{{ knp_pagination_sortable(pagination, 'Horario', 'g.horario') }}</th>
                     <th>{{ knp_pagination_sortable(pagination, 'Horario Fin', 'g.horarioFin') }}</th>
                     <th>{{ knp_pagination_sortable(pagination, 'Salon', 'g.salon') }}</th>
                     <th>{{ knp_pagination_sortable(pagination, 'Docente', 'g.iddocente') }}</th>
                     #}
                   </tr>
               </thead>
               <tbody>
               {% for grupo in pagination %}
                          <tr>
                             <td>{{ grupo.tipo }}</td>
                             <td>{{ grupo.nivel }}</td>
                             <td>{% if grupo.horario %}{{ grupo.horario|date(' H:i:s') }}{% endif %}</td>
                             <td>{% if grupo.horarioFin %}{{ grupo.horarioFin|date(' H:i:s') }}{% endif %}</td>
                             <td>{{ grupo.salon }}</td>
                             {#<td>{{ grupo.iddocente}}#}
                             <td>{{ grupo.coach}}
                               <td>
                                        <a href=\"{{ path('grupo_show', { 'id': grupo.id }) }}\" > <img src=\"{{asset('vendor/imagenes/ver.ico')}}\" alt=\"Ver\" class=\"ico\"></a>
                                        <a href=\"{{ path('grupo_edit', { 'id': grupo.id }) }}\" > <img src=\"{{asset('vendor/imagenes/edit.ico')}}\" alt=\"Editar\" class=\"ico\"></a>
                                        <a href=\"{{ path('grupo_listadoGrupo', { 'id': grupo.id }) }}\"><img src=\"{{asset('vendor/imagenes/list-user.png')}}\" title=\"Lista de Alumnos\" alt=\"Ver\" class=\"ico\"> </a>
                                        {#<a href=\"{{ path('evaluacion_new') }}\" > <img src=\"{{asset('vendor/imagenes/exam.png')}}\" alt=\"examen\" class=\"ico\"></a>#}
                                        <a href=\"{{ path('evaluacion_crear',{'id':grupo.id}) }}\" > <img src=src=\"{{asset('vendor/imagenes/exam.png')}}\"  alt=\"examen\" class=\"ico\"></a>
                               </td>
                         </tr>
               {% endfor %}
               </tbody>
             </table>
             <div class=\"navigation\">
                {{ knp_pagination_render(pagination) }}
             </div>
       </div>
     </div><!-- /.row -->
    </div><!-- /.container-fluid -->

  </div>
</div>
{% endblock %}
", "grupo/index.html.twig", "C:\\Users\\Christian\\Documents\\dsi2\\proy\\2sprint\\Nueva carpeta\\Befluent\\app\\Resources\\views\\grupo\\index.html.twig");
    }
}
