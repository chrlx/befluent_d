<?php

/* security/login.html.twig */
class __TwigTemplate_bd82d7c6c004e411d2ab6717e2c9c61850e372859524ca00551b21a7ff177803 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("principal.html.twig", "security/login.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "principal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/css/signin.css"), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "\t";
        $this->displayParentBlock("body", $context, $blocks);
        echo "

  <div class=\"cover\" style=\"background-image: url(";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("vendor/imagenes/loginFont.jpg"), "html", null, true);
        echo ");\">
    <div class=\"container\">
  \t<form action=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("academiainscripcion_login_check");
        echo "\" method=\"post\"  class=\"full-box logInForm\">
  \t\t<p class=\"text-center text-muted\"><i class=\"zmdi zmdi-account-circle zmdi-hc-5x\"></i></p>
  \t\t<p class=\"text-center text-muted text-uppercase\">";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Inicia sesión con tu cuenta"), "html", null, true);
        echo "</p>
      ";
        // line 16
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 16, $this->source); })())) {
            // line 17
            echo "      <div class=\"text-danger\">
      </div>
      ";
        }
        // line 20
        echo "  \t\t<div class=\"form-group label-floating\">
  \t\t  <label class=\"control-label\" for=\"UserEmail\">";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Usuario"), "html", null, true);
        echo ":</label>
        <input type=\"text\" id=\"username\" class=\"form-control\" name=\"_username\" required autofocus />

  \t\t  <p class=\"help-block\">Escribe tú Usuario</p>
  \t\t</div>
  \t\t<div class=\"form-group label-floating\">
  \t\t  <label class=\"control-label\" for=\"UserPass\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Contraseña"), "html", null, true);
        echo "</label>
        <input type=\"password\" id=\"password\" class=\"form-control\" name=\"_password\" required />
  \t\t  <p class=\"help-block\">Escribe tú contraseña</p>
  \t\t</div>
  \t\t<div class=\"form-group text-center\">
        <input type=\"hidden\" name=\"_target_path\" value=\"academiainscripcion_homepage\" />
  \t\t\t<input type=\"submit\" value=\"Iniciar sesión\" class=\"btn btn-raised btn-danger\">
  \t\t</div>
  \t</form>

  </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 27,  95 => 21,  92 => 20,  87 => 17,  85 => 16,  81 => 15,  76 => 13,  71 => 11,  65 => 9,  59 => 8,  50 => 5,  45 => 4,  39 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'principal.html.twig' %}

{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href=\"{{ asset('vendor/css/signin.css') }}\">
{% endblock %}

{% block body %}
\t{{ parent() }}

  <div class=\"cover\" style=\"background-image: url({{ asset('vendor/imagenes/loginFont.jpg')}});\">
    <div class=\"container\">
  \t<form action=\"{{ path('academiainscripcion_login_check') }}\" method=\"post\"  class=\"full-box logInForm\">
  \t\t<p class=\"text-center text-muted\"><i class=\"zmdi zmdi-account-circle zmdi-hc-5x\"></i></p>
  \t\t<p class=\"text-center text-muted text-uppercase\">{{'Inicia sesión con tu cuenta'|trans}}</p>
      {% if error %}
      <div class=\"text-danger\">
      </div>
      {% endif %}
  \t\t<div class=\"form-group label-floating\">
  \t\t  <label class=\"control-label\" for=\"UserEmail\">{{'Usuario'|trans}}:</label>
        <input type=\"text\" id=\"username\" class=\"form-control\" name=\"_username\" required autofocus />

  \t\t  <p class=\"help-block\">Escribe tú Usuario</p>
  \t\t</div>
  \t\t<div class=\"form-group label-floating\">
  \t\t  <label class=\"control-label\" for=\"UserPass\">{{'Contraseña'|trans}}</label>
        <input type=\"password\" id=\"password\" class=\"form-control\" name=\"_password\" required />
  \t\t  <p class=\"help-block\">Escribe tú contraseña</p>
  \t\t</div>
  \t\t<div class=\"form-group text-center\">
        <input type=\"hidden\" name=\"_target_path\" value=\"academiainscripcion_homepage\" />
  \t\t\t<input type=\"submit\" value=\"Iniciar sesión\" class=\"btn btn-raised btn-danger\">
  \t\t</div>
  \t</form>

  </div>
{% endblock %}
", "security/login.html.twig", "C:\\Users\\Christian\\Documents\\dsi2\\proy\\2sprint\\Nueva carpeta\\Befluent\\app\\Resources\\views\\security\\login.html.twig");
    }
}
